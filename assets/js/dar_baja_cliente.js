var dar_baja_cliente = (function() {
	var base_url = window.location.origin + '/floreria/';
	
	var $ventanaModal;
	var $btn_aceptar;
	var $btn_cancelar;
	var $contenedor_modal;
	var $my_id;
	
	var dni;	
	
	var inicializar = function () {
		$ventanaModal = $('#ventanaModal');
		$btn_aceptar = $('#aceptar');
		$btn_cancelar = $('#cancelar');
		$contenedor_modal = $('#contenedor_modal');
		$my_id = $('#myID');
	};
	
	var setearVentanaModal = function(msg_title, msg_body) {
		$ventanaModal.find('.modal-title').text(msg_title);
		$ventanaModal.find('.modal-body').text(msg_body);
	};
	
	var obtenerClienteLinea = function($this, event) {
		var linea = $this.parent().parent();

		console.log("linea:");
		
		var dni = linea.children(":first");
		console.log(dni.text());
		var nombre = dni.next();
		var apellido = nombre.next();
		var email = apellido.next();
		var direccion = email.next();
		var telefono = direccion.next();
		
		return { 
			dni : dni.text(),
			nombre : nombre.text(),
			apellido : apellido.text()
		}
	};
	
	var btn_eliminar_click = function($this, event) {		
		console.log("eliminar");

		console.log($this.children().text());
		
		var cliente = obtenerClienteLinea($this);
		
		var text_title = "Baja de cliente";
		var text_body = "¿Estás seguro de dar de baja a "+ cliente.nombre + " " + 
						cliente.apellido + " DNI:" + cliente.dni + " ?";
		
		setearVentanaModal(text_title, text_body);
		
		$contenedor_modal.off();
		$contenedor_modal.on('click', '.btn-primary', function(event) {
			confirmar_baja_click(cliente.dni);
		});
	};
	
	var cliente_post = function(cliente){
		var text_title = "Mensaje de Información";
		if (cliente == "si") {
			text_body = "La transacción ha sido exitosa";
		} else {
			text_body = "Ha ocurrido un error durante la transacción";
		}
		setearVentanaModal(text_title, text_body);
	};
	
	var confirmar_baja_click = function(dni) {
		$.post(base_url + 'Fachada_cliente/bajar_cliente',
			{ dni : dni },
			function(cliente){
				cliente_post(cliente);
			}
		);
		$contenedor_modal.off();
		$contenedor_modal.on('click', '.btn-primary', function(event) {
			$ventanaModal.modal('toggle');
			window.location.reload(true);
		});
	};
	
	var binds = function() {
		console.log("binds");
		
		$my_id.on('click', '.btn-danger', function(event) {
			event.preventDefault();
			var $this = $(this);
			console.log("bind eliminar");
			btn_eliminar_click($this, event);
		});
		
		$contenedor_modal.on('click', '.btn-primary', function(event) {
			confirmar_baja_click;
		});
	};
	
	var init = function() {
		console.log("init");
		inicializar();
		binds();
	};
	
	return { init : init }
})();

$(document).ready(function() {
	function cargar_tabla(result) {
		$("#myID").html(result);
		table = $('#tablaInventario').DataTable({
			"retrieve":			true,
			"scrollY":			"200px",
			"scrollCollapse":	true,
			"paging":			true
		});
	}
	
	$.get("<?php echo base_url() ?>Fachada_cliente/buscar_clientes/Eliminar/danger", 
			function(result) { 
				cargar_tabla(result) 
			}
	);

	dar_baja_cliente.init();
});