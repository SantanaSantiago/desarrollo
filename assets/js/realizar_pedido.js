/**
 * @namespace
 */
var realizar_pedido = (function() {
	var base_url = window.location.origin + '/sgf/';
	var $pestañas;
	var $contenedor_productos;
	var $contenedor_carrito;
	var $comprar;
	var sucursal;
	
	/**
	@memberOf realizar_pedido
	*/
	var inicializar = function () {
		$pestañas = $('#tabs');
		$contenedor_productos = $('#contenedor_productos');
		$contenedor_carrito = $('#contenedor_carrito');
		$comprar = $('#comprar');
		sucursal = $('#tabs').find('.active').text();
	};

	/**
	 * @memberOf realizar_pedido
	 */
	var cargar_productos = function (productos, func) {
		$.post(base_url + 'Fachada_pedido/mostrar_productos',
			{productos: productos,
			sucursal: sucursal}).done(
			function(producto) {
				$contenedor_productos.html(producto);
				validation();
				//$(".form-control").mask('0000');
			});
	};
	
	/**
	 * @memberOf realizar_pedido
	 */
	var obtener_stock = function(producto, async) {
		return $.post(base_url + 'Fachada_pedido/obtener_stock', { 
			sucursal : sucursal, 
			producto : producto 
		});
	};
	
	/**
	 * @memberOf realizar_pedido
	 */
	var toggle_comprar = function() {
		$.post(base_url + 'Fachada_pedido/contar_productos_carrito',
				{ sucursal : sucursal },
				function(cant_productos) {
					console.log("cant_productos: " + cant_productos);
					if (cant_productos >= 1) {
						$comprar.attr('disabled', false);
					} else {
						$comprar.attr('disabled', true);
					}
				}
		)
	};

	/**
	 * @memberOf realizar_pedido
	 */
	var mostrar_carrito = function (sucursal, func) {
		$.post(base_url + 'Fachada_pedido/mostrar_carrito',
			{sucursal: sucursal},
			function(carrito) {
				console.log(carrito);
				$contenedor_carrito.html(carrito);
			}
		);
		toggle_comprar();
	};
	
	/**
	 * @memberOf realizar_pedido
	 */
	var mostrar_productos = function (sucursal) {
		$.getJSON(base_url 
					+ 'Fachada_producto/buscar_productos_sucursal/' 
					+ sucursal, 
				function(productos) {
					if( productos.length > 0 )
					{
						cargar_productos(productos);
					} else {
						$contenedor_productos.html("");
					}
			}
		);
	};
	
	/**
	 * @memberOf realizar_pedido
	 */
	var añadir_a_carrito = function (producto, cantidad) {
		return $.post(base_url + 'Fachada_pedido/agregar_producto_carrito/', {
			producto: producto, 
			cantidad: cantidad, 
			sucursal: sucursal
		}); 
	};

	/**
	 * @memberOf realizar_pedido
	 */
	var agregar_producto_carrito = function ($this, event) {
		var producto = $this.attr('id').split('agregar-')[1];
		var $form = $('#form' + producto);
		var $input = $form.find("input");
		var $quedan = $('#quedan-' + producto);
		
		var cantidad = $input.val();
		
		if (cantidad == 0) {
			return "error";
		}
		
		obtener_stock(producto).done(function(stock) {
			añadir_a_carrito(producto, cantidad)
			.done(function(result) {
				if (result != "Stock Insuficiente") {
					mostrar_carrito(sucursal);
				
					if (stock == cantidad) {
						$this.attr('disabled', true);
						$input.attr('disabled', true);
					}
					
					// actualizo stock
					nuevo_stock = stock - cantidad;
					
					$input.attr('max', nuevo_stock);
					$input.val("");
					$quedan.html("<strong>¡Quedan " + nuevo_stock + ' unidades!</strong>');
				} else {
					alert("El stock disponible es: " + stock);
					$quedan.html("<strong>¡Quedan " + stock + ' unidades!</strong>');
				}
			})
		});
	};

	/**
	 * @memberOf realizar_pedido
	 */
	var quitar_producto_carrito = function($this, event) {
		var $linea = $this.parent().parent();
		var producto = $linea.children(":first").text();
		var $producto = $('#' + producto);
		var $input = $('input[alt=' + producto + ']');
		var $quedan = $('#quedan-' + producto);
		
		$linea.remove();
		
		$.post(base_url + 'Fachada_pedido/quitar_producto_carrito/',
			{producto: producto, sucursal: sucursal},
			function() {
				toggle_comprar();
				obtener_stock(producto).done(function(stock) {
					console.log("stock: " + stock);
					$input.attr('max', stock);
					$quedan.html("<strong>¡Quedan " + stock +  " unidades!</strong>");
					actualizar_total_carrito();
				});
			}
		);
					
		$('#agregar-' + producto).attr('disabled', false);
		$input.attr('disabled', false);
	};
	
	/**
	 * @memberOf realizar_pedido
	 */
	var crear_pedido = function() {
		$.post(base_url + 'Fachada_pedido/crear_pedido',
				{ sucursal : sucursal },
				function(pedido) {
					if (pedido === 'cliente') {
						window.location.reload();
					} else {
						window.location.href = base_url + 'Fachada_pedido/ver_pedido/' + pedido;
					}
				}
		);
	};
	
	/**
	 * @memberOf realizar_pedido
	 */
	var pestaña_click = function($this) {
		sucursal = $this.text();
		console.log("sucursal: " + sucursal);
		mostrar_productos(sucursal);
		mostrar_carrito(sucursal);
		toggle_comprar();
	};
	
	var actualizar_total_carrito = function() {
		$.post(base_url + 'Fachada_pedido/obtener_total_carrito',
		{sucursal: sucursal},
		function(total) {
			$("#total").html('<strong>$' + total + '</strong>');
		});
	};
	
	/**
	 * @memberOf realizar_pedido
	 */
	var validation = function() {
		$("form").each(function () { 
			$this = $(this);
			
			$this.validate({
				messages: {
					cantidad: {
						max: 'El valor introducido supera el stock disponible'
					}
				},
			});

			$this.find('input').rules('remove', 'max');
		});
	};
	
	/**
	 * @memberOf realizar_pedido
	 */
	var binds = function() {
		$pestañas.on('click', 'li', function(event) {
			var $this = $(this);
			pestaña_click($this);
		});
	
		$comprar.click(function() {
			console.log("crear_pedido");
			crear_pedido();
		});
		
		$contenedor_productos.on('click', '.btn-primary', function(event) {
			event.preventDefault();
			var $this = $(this);
			agregar_producto_carrito($this, event);
		});
	
		$contenedor_carrito.on('click', '.btn-danger', function(event) {
			event.preventDefault();
			var $this = $(this);
			quitar_producto_carrito($this, event);
		});
		
	}
	
	/**
	 * @memberOf realizar_pedido
	 */
	var init = function() {
		inicializar();
		mostrar_productos(sucursal);
		mostrar_carrito(sucursal);
		toggle_comprar();
		binds();
	};
	
	return { init : init }
})();

$(document).ready(function() {
	realizar_pedido.init();

	jQuery.validator.setDefaults({
		debug: true,
		success: "valid"
	});	
});
