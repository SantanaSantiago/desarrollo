<?php

class Movimiento extends DataMapper {

	var $table = 'movimiento';
	var $has_one = array('cuenta_corriente');
	var $has_many = array('movimiento_has_venta', 'contrato_has_movimiento');

	var $validation = array(
		'fecha_hora' => array(
			'label' => 'Fecha y Hora',
			'rules' => array('required')
		),
		'importe' => array(
			'label' => 'Importe',
			'rules' => array('required')
		),
		'tipo_movimiento' => array(
			'label' => 'Tipo de Movimiento',
			'rules' => array('required')
		)
	);

	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE)
	{
	}
}


