<?php
class Rol extends DataMapper {
	var $table = 'rol';
	var $has_many = array (
		"usuario",
	);
	
	var $validation = array (
		'id' => array (
			'label' => 'ID',
			'rules' => array ()
		),
		'nombre_rol' => array (
			'label' => 'Nombre del Rol',
			'rules' => array (
				'required',
				'unique'
			)
		),
	);
	
	function __construct($id = NULL) {
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE) {
	}
}
