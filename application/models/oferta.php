<?php

class Oferta extends DataMapper {
	
	var $table = 'oferta';	
	var $has_one = array('lote');

	var $validation = array(
		'fecha_inicio' => array(
			'label' => 'Fecha de Inicio',
			'rules' => array('required')
		),
		'fecha_fin' => array(
			'label' => 'Fecha de Fin',
			'rules' => array('required')
		),
		'descuento' => array(
			'label' => 'Descuento',
			'rules' => array('required')
		),
	);
	
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE)
	{
	}
}

