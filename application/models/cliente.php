<?php
class Cliente extends DataMapper {
	var $table = 'cliente';
	var $has_many = array (
		"pedido",
		"cuenta_corriente",
		"carrito"
	);
	
	var $has_one = array('usuario');
	
	var $validation = array (
		'id' => array (
			'label' => 'ID',
			'rules' => array ()
		),
		'dni' => array (
			'label' => 'DNI',
			'rules' => array (
				'required',
				'unique'
			)
		),
		'nombre' => array (
			'label' => 'Nombre',
			'rules' => array (
				'required'
			)
		),
		'apellido' => array (
			'label' => 'Apellido',
			'rules' => array (
				'required'
			)
		),
		'direccion' => array (
			'label' => 'Direccion',
			'rules' => array (
				'required'
			)
		),
		'telefono' => array (
			'label' => 'Telefono',
			'rules' => array (
				'required'
			)
		),
		'habilitado' => array(
			'label' => 'Habilitado',
			'rules' => array('required')
		),
		'usuario_id' => array(
			'label' => 'Id de Usuario',
			'rules' => array('required')
		)
	);

	public function guardar($dni, $nombre, $apellido, $direccion, $telefono, $mail, $password) {
		$this->dni = $dni;
		$this->nombre = $nombre;
		$this->apellido = $apellido;
		$this->direccion = $direccion;
		$this->telefono = $telefono;
		$this->habilitado = 1;
		
		$usuario = new Usuario();
		$usuario->mail = $mail;
		$usuario->clave_de_acceso = $password;
		$usuario->rol_id = 1;
		
		if ($usuario->save()) {
			$this->usuario_id = $usuario->id;
			if ($this->save()) {
				return TRUE;
			}
		} 
		
		return FALSE;
	}
	
	public function get_datos_cliente($dni) {
		$this->get_by_dni($dni);
		$usuario = new Usuario();
		$usuario->get_by_id($this->usuario_id);
		$datos = array(
			'nombre' => $this->nombre,
			'apellido' => $this->apellido,
			'email' => $usuario->mail,
			'direccion' => $this->direccion,
			'telefono' => $this->telefono
		);
		return $datos;
		
	}
	
	function __construct($id = NULL) {
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE) {
	}
}
