<?php

class Sucursal extends DataMapper {
	var $table = 'sucursal';
	var $has_many = array('inventario','pedido');

	var $validation = array(
		'id' => array(
			'label' => 'ID',
			'rules' => array('required')
		),
		'nombre_sucursal' => array(
			'label' => 'Nombre de Sucursal',
			'rules' => array('required')
		),
		'direccion' => array(
			'label' => 'Direccion',
			'rules' => array('required')
		),
		'telefono' => array(
			'label' => 'Telefono',
			'rules' => array('required')
		),
		'mail' => array(
			'label' => 'Mail',
			'rules' => array('required')
		)
	);
	
	function obtener_pedidos($nombre_sucursal) {
		$pedido = new Pedido();
		$pedido->where_related('sucursal', 'nombre_sucursal', $nombre_sucursal);
		$pedido->where_related('venta', 'id', NULL);
		return $pedido->get();
	}


	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE)
	{
	}
}

