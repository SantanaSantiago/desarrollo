<?php
class Inventario extends DataMapper {
	var $table = 'inventario';
	var $has_one = array('producto', 'sucursal');
	var $has_many = array('lote', 'entrega', 'carrito');
	
	var $validation = array(
		'id' => array(
			'label' => 'ID',
			'rules' => array()
		),
		'stock' => array(
			'label' => 'Stock de Inventario',
			'rules' => array('required')
		),
		'producto_id' => array(
			'label' => 'ID de Product',
			'rules' => array()
		),
		'sucursal_id' => array(
			'label' => 'ID de Sucursal',
			'rules' => array('required')
		)
	);
	
	public function obtener_stock($nombre_sucursal, $nombre_producto) {
		$this->where_related('sucursal', 'nombre_sucursal', $nombre_sucursal);
		$this->where_related('producto', 'nombre_producto', $nombre_producto);
		$this->get();
		
		return $this->stock;
	}
	
	public function obtener_precio($inventario_id) {
		$producto = new Producto();
		$producto->where_related('inventario', 'id', $inventario_id)->get();
		
		return $producto->obtener_precio($producto);
	}
	
	public function disminuir_stock($nombre_sucursal, $nombre_producto, $cantidad) {
		$stock = $this->obtener_stock($nombre_sucursal, $nombre_producto);
		$nuevo_stock = $stock - $cantidad;
		
		$sucursal = new Sucursal();
		$id_sucursal = $sucursal->where('nombre_sucursal', $nombre_sucursal)->get()->id;
		
		$producto = new Producto();
		$id_producto = $producto->where('nombre_producto', $nombre_producto)->get()->id;
		
		$this->where('sucursal_id', $id_sucursal);
		$this->where('producto_id', $id_producto);
		$this->update('stock', $nuevo_stock);
	}

	function __construct($id = NULL) {
		parent::__construct($id);
	}
	
	function post_model_init($from_cache = FALSE) {
	}
}


