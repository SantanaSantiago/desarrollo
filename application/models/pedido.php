<?php
class Pedido extends DataMapper {
	
	var $table = 'pedido';	
	var $has_one = array('cliente', 'venta', 'sucursal');
	var $has_many = array('linea_pedido');

	var $validation = array(
		'id' => array(
			'label' => 'ID',
			'rules' => array()
		),
		
		'cliente_id' => array(
				'label' => 'ID cliente',
				'rules' => array('required')
		),
		'importe' => array(
			'label' => 'Importe',
			'rules' => array()
		),
		'fecha_hora' => array(
			'label' => 'Fecha y Hora',
			'rules' => array()
		),
		
		'valor_iva' => array(
			'label' => 'Valor de Iva',
			'rules' => array('required')
		),
		'sucursal_id' => array(
			'label' => 'ID de Sucursal',
			'rules' => array('required')
		)
	);
	
	public function crear_pedido($cliente_id, $sucursal_id) {
		$this->cliente_id = $cliente_id;
		$this->valor_iva = 21;
		$this->sucursal_id = $sucursal_id;
		
		$this->save();
		
		$id = $this->db->insert_id();
		$this->get_by_id($id);
		
		$importe = $this->calcular_importe($id);
		$this->where('id', $id)->update('importe', $importe);
		
		return $id; 
	}
	
	public function obtener_precio($precio_unitario, $cantidad) {
		bcscale(2);
		$precio_iva = bcmul($precio_unitario, "1.21");
		$precio_total = bcmul($precio_iva, $cantidad);
		return $precio_total;
	}
	
	public function calcular_importe($id) {
		$linea_pedido = new Linea_pedido();
		
		$this->where('id', $id)->get();
		$linea_pedido->where_related($this)->get();
		
		$importe = "0";
		foreach ($linea_pedido as $producto) {
			$precio_unitario = $producto->precio_producto;
			$cantidad = $producto->cantidad;
			bcscale(2);
			$precio_linea = bcmul($precio_unitario, $cantidad, 2);
			$importe = bcadd($importe, $precio_linea);
		}
		return $importe;
	}
	
	function __construct($id = NULL) {
		parent::__construct($id);
	}
	
	function post_model_init($from_cache = FALSE) {
	}
}
