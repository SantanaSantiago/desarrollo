<?php

class Producto extends DataMapper {
	
	var $table = 'producto';	

	var $has_many = array(
		'linea_pedido',
		'inventario',
		'arreglo' => array(
			'class' => 'producto',
			'other_field' => 'producto',
			'join_self_as' => 'ARREGLO',
			'join_other_as' => 'PRODUCTO',
			'join_table' => 'Arreglo'
		),
		'producto_en_arreglo' => array(
			'class' => 'producto',
			'other_field' => 'producto',
			'join_self_as' => 'PRODUCTO',
			'join_other_as' => 'ARREGLO',
			'join_table' => 'Arreglo'
		)
	);

	var $validation = array(
		'id' => array(
			'label' => 'ID',
			'rules' => array()
		),
		'precio' => array(
			'label' => 'Precio',
			'rules' => array('required')
		),
		'nombre_producto' => array(
			'label' => 'Nombre_Producto',
			'rules' => array('required', 'unique')
		),
		'descripcion' => array(
			'label' => 'Descripcion',
			'rules' => array('required')
		),
		'tipo_producto' => array(
			'label' => 'Tipo de Producto',
			'rules' => array('required')
		),
	);
	
	function obtener_precio($producto) {
		$p = new Pedido();
		$precio = $p->obtener_precio($producto->precio, 1);
		return $precio;
	}
	
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE)
	{
	}
}
