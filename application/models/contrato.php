<?php

class Contrato extends DataMapper {
	
	var $table = 'contrato';	
	var $has_one = array('cuenta_corriente');
	var $has_many = array('contrato_has_movimiento', 'entrega');

	var $validation = array(
		'fecha_inicio' => array(
			'label' => 'Fecha de Inicio',
			'rules' => array('required')
		),
		'fecha_vencimiento' => array(
			'label' => 'Fecha de Vencimiento',
			'rules' => array('required')
		),
		'coste_mensual' => array(
			'label' => 'Coste Mensual',
			'rules' => array('required')
		),
		'direccion' => array(
			'label' => 'Direccion',
			'rules' => array('required')
		)
	);
	
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE)
	{
	}
}
