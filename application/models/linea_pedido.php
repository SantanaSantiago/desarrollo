<?php
class Linea_pedido extends DataMapper {
	var $table = 'linea_pedido';	
	var $has_one = array('pedido', 'producto');

	var $validation = array(
		'id' => array(
			'label' => 'ID',
			'rules' => array('required')
		),
		'precio_producto' => array(
			'label' => 'Precio de Producto',
			'rules' => array('required')
		),
		'cantidad' => array(
			'label' => 'Cantidad',
			'rules' => array('required')
		),
		'pedido_id' => array (
			'label' => 'pedido_id',
			'rules' => array () 
		),

		'producto_id' => array(
			'label' => 'ID de Producto',
			'rules' => array('required')
		)
	);
	
	function obtener_pedido($id) {
		return $this->get_by_related('pedido', 'id', $id);
	}
	
	function __construct($id = NULL) {
		parent::__construct($id);
	}
	function post_model_init($from_cache = FALSE) {
	}
}
