<?php
class Usuario extends DataMapper {
	var $table = 'usuario';
	
	var $has_one = array('cliente', 'rol');
	
	var $validation = array (
		'id' => array (
			'label' => 'ID',
			'rules' => array ()
		),
		'mail' => array (
			'label' => 'Mail',
			'rules' => array (
				'required',
				'unique'
			)
		),
		'clave_de_acceso' => array (
			'label' => 'Clave',
			'rules' => array (
				'required'
			)
		),
		'rol_id' => array (
			'label' => 'Rol',
			'rules' => array (
				'required'
			)
		),
	);

	public function guardar($mail, $clave, $rol) {
		$this->mail = $mail;
		$this->password = $clave;
		
		$r = new Rol();
		$r->where('nombre_rol', $rol)->get();
		
		$this->rol_id = $r->id;
		
		if ($this->save()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function login($username, $password) {
		$this->where('mail', $username);
		$this->where('clave_de_acceso', $password);
		$this->get();
		
		if ($this->exists()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function __construct($id = NULL) {
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE) {
	}
}
