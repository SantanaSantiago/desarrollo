<?php

class Venta extends DataMapper {
	
	var $table = 'venta';	
	var $has_one = array('pedido');

	var $validation = array(
		'id' => array(
			'label' => 'ID Venta',
			'rules' => array()
		),	
		'fecha_hora_venta' => array(
			'label' => 'Fecha y Hora de Venta',
			'rules' => array()
		),
		'pedido_id' => array(
			'label' => 'Id de Pedido',
			'rules' => array('required')
		)
	);
	
	function crear_venta($id_pedido) {
		$pedido = new Pedido();
		$pedido->get_by_id($id_pedido);
		$this->pedido_id = $pedido->id;
		
		$call = 'CALL sp_actualizar_lotes_venta(' . $this->pedido_id . ')';
		
		if ($this->save()) {
			echo "genial";
		} else {
			echo "error al cargar venta";
		}
		$this->db->query($call);
	}
	
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE)
	{
	}
}
