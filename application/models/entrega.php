<?php

class Entrega extends DataMapper {

	var $table = 'entrega';
	var $has_one = array('contrato', 'inventario');
	
	var $validation = array(
		'fecha_entrega' => array(
			'label' => 'Fecha de Entrega',
			'rules' => array('required')
		)
	);

	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE)
	{
	}
}