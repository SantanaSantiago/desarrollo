<?php
class Carrito extends DataMapper {
	var $table = 'carrito';
	var $has_one = array (
		"cliente", 
		"inventario"
	);
	
	var $validation = array (
		
		'id' => array (
			'label' => 'ID',
			'rules' => array ()
		),
		'cliente_id' => array (
			'label' => 'ID de Cliente',
			'rules' => array (
				'required'
			)
		),
		'inventario_id' => array (
			'label' => 'ID de Inventario',
			'rules' => array (
				'required'
			)
		),
		'cantidad' => array(
			'label' => 'Cantidad',
			'rules' => array('required')
		)
	);

	public function guardar($cliente_id, $inventario_id, $cantidad) {
		$this->cliente_id = $cliente_id;
		$this->inventario_id = $inventario_id;
		$this->cantidad = $cantidad;
		$this->save();
	}
	
	public function contar_productos($nombre_sucursal, $cliente_id) {
		$this->where_related('inventario/sucursal', 'nombre_sucursal', $nombre_sucursal);
		$this->where_related('cliente', 'id', $cliente_id);
		
		return $this->count();
	}
	
	public function obtener_total($nombre_sucursal, $cliente_id) {
		$this->where_related('inventario/sucursal', 'nombre_sucursal', $nombre_sucursal);
		$this->where('cliente_id', $cliente_id);
		$this->get();
		
		$total = "0.00";
		foreach($this as $carrito) {
			$p = new Pedido();
			$cantidad = $carrito->cantidad;
			$producto = $carrito->inventario->get()->producto->get();
			$precio = $p->obtener_precio($producto->precio, $cantidad);
			bcscale(2);
			$total = bcadd($total, $precio);
		}
		return $total;
	}

	function __construct($id = NULL) {
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE) {
	}
}
