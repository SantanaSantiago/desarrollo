<?php

class Cuenta_corriente extends DataMapper {
	
	var $table = 'cuenta_corriente';	
	var $has_one = array('cliente');
	var $has_many = array('contrato');

	var $validation = array(
		'limite_credito' => array(
			'label' => 'Limite de Credito',
			'rules' => array('required')
		),
		'saldo' => array(
			'label' => 'Saldo',
			'rules' => array('required')
		)
	);
	
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE)
	{
	}
}


