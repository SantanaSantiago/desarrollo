<?php
class Lote extends DataMapper {
	var $table = 'lote';	
	var $has_many = array('oferta');
	var $has_one = array('inventario');
	
	var $validation = array (
			'id' => array (
					'label' => 'ID',
					'rules' => array () 
			),
			'stock_lote' => array (
					'label' => 'Stock de Lote',
					'rules' => array (
							'required' 
					) 
			),
			'fecha_ingreso' => array (
					'label' => 'Fecha de Ingreso',
					'rules' => array () 
			),
			'fecha_vencimiento' => array (
					'label' => 'Fecha de Vencimiento',
					'rules' => array () 
			),
			'oferta_id' => array (
					'label' => 'oferta_id',
					'rules' => array ('required')
			),
			'inventario_id' => array (
					'label' => 'inventario_id',
					'rules' => array ('required')
			)
	);
	function __construct($id = NULL) {
		parent::__construct($id);
	}
	function post_model_init($from_cache = FALSE) {
	}
}

