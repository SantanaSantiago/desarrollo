<?php

class Movimiento_has_venta extends DataMapper {

	var $table = 'movimiento_has_venta';
	var $has_one = array('movimiento', 'venta');

	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	function post_model_init($from_cache = FALSE)
	{
	}
}