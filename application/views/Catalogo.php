
<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="suc-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $titulo_catalogo;?></h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<div class="thumbnail">
										<img alt="Bootstrap Thumbnail Second"
											src=<?php echo $img1;?> />
										<div class="caption">
											<h3> <?php echo $title;?> </h3>
											<form class="form-horizontal" role="form">
												<div class="form-group">
													<label for="cantidad" class="col-lg-5 control-label">Cantidad</label>
													<div class="col-lg-7">
														<input type="number" class="form-control" id="cantidad">
													</div>
												</div>
												<div class="form-group">
													<div class="btn btn-primary pull-right" href="#">
														<span class="glyphicon glyphicon-plus-sign"></span>
														Agregar
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="thumbnail">
										<img alt="Bootstrap Thumbnail Second"
											src=<?php echo $img2;?> />
										<div class="caption">
											<h3> <?php echo $title;?> </h3>
											<form class="form-horizontal" role="form">
												<div class="form-group">
													<label for="cantidad" class="col-lg-5 control-label">Cantidad</label>
													<div class="col-lg-7">
														<input type="number" class="form-control" id="cantidad">
													</div>
												</div>
												<div class="form-group">
													<div class="btn btn-primary pull-right" href="#">
														<span class="glyphicon glyphicon-plus-sign"></span>
														Agregar
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="thumbnail">
										<img alt="Bootstrap Thumbnail Second"
											src=<?php echo $img3;?> />
										<div class="caption">
											<h3> <?php echo $title;?> </h3>
											<form class="form-horizontal" role="form">
												<div class="form-group">
													<label for="cantidad" class="col-lg-5 control-label">Cantidad</label>
													<div class="col-lg-7">
														<input type="number" class="form-control" id="cantidad">
													</div>
												</div>
												<div class="form-group">
													<div class="btn btn-primary pull-right" href="#">
														<span class="glyphicon glyphicon-plus-sign"></span>
														Agregar
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="thumbnail">
										<img alt="Bootstrap Thumbnail Second"
											src=<?php echo $img4;?> />
										<div class="caption">
											<h3> <?php echo $title;?> </h3>
											<form class="form-horizontal" role="form">
												<div class="form-group">
													<label for="cantidad" class="col-lg-5 control-label">Cantidad</label>
													<div class="col-lg-7">
														<input type="number" class="form-control" id="cantidad">
													</div>
												</div>
												<div class="form-group">
													<div class="btn btn-primary pull-right" href="#">
														<span class="glyphicon glyphicon-plus-sign"></span>
														Agregar
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							<div class="text-center">
								<ul class="pagination">
									<li><a href="#">&laquo;</a></li>
									<li><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li><a href="#">&raquo;</a></li>
								</ul>
							</div>
						</div>
						<!-- Panel Body -->
					</div>
			</div>
			<!-- Suc-1 -->
		</div>
	</div>
</div>