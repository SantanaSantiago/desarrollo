<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2 id="titulo" class="text-center"><span class="glyphicon glyphicon-user"></span> Clientes</h2>
			</div>
			<table class="table table-hover dt-responsive" id="tablaClientes">
				<thead>
					<tr>
						<th><strong>DNI</strong></th>
						<th><strong>Nombre</strong></th>
						<th><strong>Apellido</strong></th>
						<th><strong>Email</strong></th>
						<th><strong>Dirección</strong></th>
						<th><strong>Teléfono</strong></th>
					</tr>
				</thead>
				<tbody id="myID">
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {

	var url = "<?php echo base_url(); ?>Tablas/clientes";

	table = $('#tablaClientes').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": url,
				"type": "POST"
			},
			"columns" : [
				null,
				null,
				null,
				null,
				null,
				null,
			]
		});
});
</script>
<style>
@font-face {
    font-family: 'myfont';
    src: url(/sgf/assets/fonts/Arciform.otf);
}
#titulo{
font-family: myfont;
}

</style>