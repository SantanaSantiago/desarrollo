<link href="<?php echo base_url(); ?>assets/css/styles.css"
	rel="stylesheet">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-9">
			<div id="wrapper">
				<ul class="list-group">
					<li class="list-group-item"><?php
					$data = array (
							'titulo_catalogo' => 'Flores Naturales',
							'img1' => '/Floreria/assets/imagenes/Tulips.jpg',
							'img2' => '/Floreria/assets/imagenes/Tulips.jpg',
							'img3' => '/Floreria/assets/imagenes/Tulips.jpg',
							'img4' => '/Floreria/assets/imagenes/Tulips.jpg',
							'title'=> 'Tulipan'
					);
					
					
					$this->load->view ( 'Catalogo', $data );
					?></li>
					<li class="list-group-item"><?php
					$data = array (
							'titulo_catalogo' => 'Flores Artificiales',
							'img1' => '/proyecto/assets/imagenes/FlorArtificial.jpg',
							'img2' => '/proyecto/assets/imagenes/FlorArtificial.jpg',
							'img3' => '/proyecto/assets/imagenes/FlorArtificial.jpg',
							'img4' => '/proyecto/assets/imagenes/FlorArtificial.jpg',
							'title'=> 'Flor artificial'
					);
					
					$this->load->view ( 'Catalogo', $data );
					?></li>
					<li class="list-group-item"><?php
					$data = array (
							'titulo_catalogo' => 'Complementos',
							'img1' => '/proyecto/assets/imagenes/Jarron.jpg',
							'img2' => '/proyecto/assets/imagenes/Jarron.jpg',
							'img3' => '/proyecto/assets/imagenes/Jarron.jpg',
							'img4' => '/proyecto/assets/imagenes/Jarron.jpg',
							'title'=> 'Jarrón'
					);
					
					$this->load->view ( 'Catalogo', $data );
					?></li>
				</ul>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title-center text-center ">Productos agregados en
						el arreglo</h3>
				</div>
				<div class="panel-body">
					<div class="table-responsive"
						style="min-height: 10; max-height: 10; overflow-y: scroll;">
						<table
							class="table 
			                                  table-striped table-hover table-condensed">
							<thead>
								<tr>
									<th width="25%">Producto</th>
									<th width="25%">Cantidad</th>
									<th width="25%">Subtotal</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Flor Artificial</td>
									<td>4</td>
									<td>$ 30</td>

								</tr>
								<tr>
									<td>Tulipan</td>
									<td>7</td>
									<td>$ 85</td>
								</tr>
								<tr>
									<td>Jarrón</td>
									<td>10</td>
									<td>$ 90</td>

								</tr>
							</tbody>
						</table>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<span class="label label-default pull-right">Total $205</span>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<button type="button" class="btn btn-success btn-block">¡Crear!</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>