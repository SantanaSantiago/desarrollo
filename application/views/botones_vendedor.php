<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle colapsed"
        data-toggle="collapse" data-target="#bs-navbar-collapse"
        aria-expanded="false">
        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
        <span class="icon-bar"></span> <span class="icon-bar"></span>
      </button>
      <a style="color:rgba(138, 185, 89, 0.9)"class="navbar-brand" href="<?php echo base_url(); ?>Home/index">SGF <span class="glyphicon glyphicon-home"></span></a>
    </div>
    <div class="collapse navbar-collapse" id="bs-navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" role="button"
            id="dropdown1" aria-haspopup="true" aria-expanded="false">
            Clientes<span class="caret"></span>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdown1">
            <li><a href="<?php echo base_url(); ?>Fachada_cliente/listar"><i class="glyphicon glyphicon-search"></i> Listar</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" role="button"
            id="dropdown3" aria-haspopup="true" aria-expanded="false">
            Productos<span class="caret"></span>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdown3">
            <li><a href="<?php echo base_url(); ?>index.php/Fachada_producto/consultar_inventario"><i class="glyphicon glyphicon-list-alt"></i> Consultar Inventario</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a class="drowpdown-toggle" data-toggle="dropdown" role="button"
            id="dropdown4" aria-haspopup="true" aria-expanded="false">
            Ventas<span class="caret"></span>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdown4">
            <li><a href="<?php echo base_url(); ?>index.php/Fachada_pedido/consultar_pedidos"><i class="glyphicon glyphicon-list-alt"></i> Consultar Pedidos</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Fachada_pedido/realizar_pedido"><i class="glyphicon glyphicon-shopping-cart"></i> Realizar Pedido</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      	<li>
				<?php
				$atts = array (
						// atributos botón
						'id' => "Venta_Ayuda",
						// atributos ventana
						'width' => '800',
						'height' => '600',
						'scrollbars' => 'yes',
						'status' => 'yes',
						'resizable' => 'yes',
						'screenx' => '0',
						'screeny' => '0' 
				);
				
				$url = base_url ().'assets/help/SistemaDeGestiónDeFlorerias.html';
				
				echo anchor_popup ( $url,'Ayuda <span class="glyphicon glyphicon-question-sign"></span>',$atts );
				?>
				</li>
        <li><a style="color:rgba(255, 0, 0, 0.66)" href="<?php echo base_url(); ?>login/logout">Salir <span class="glyphicon glyphicon-log-out"> </span></a></li>
      </ul>
    </div>
  </div>
</nav>

