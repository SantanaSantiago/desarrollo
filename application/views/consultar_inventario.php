<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="titulo" class="page-header">
                <h2 class="text-center"><span class="glyphicon glyphicon-tree-deciduous"></span> Inventario</h2>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover dt-responsive" id="tablaInventario">
                    <thead>
                        <tr>
                            <th><strong>Producto</strong></th>
                            <th class="text-right"><strong>Número de Lote</strong></th>
                            <th class="text-left"><strong>Fecha de Ingreso</strong></th>
                            <th class="text-left"><strong>Fecha de Vencimiento</strong></th>
                            <th class="text-right"><strong>Stock</strong></th>
                        </tr>
                    </thead>
                    <tbody id="cuerpo_tabla">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var $tabla_inventario = $('#tablaInventario');
        var $tabs = $('#tabs');
        var sucursal = $tabs.find('.active').text();
        var url = "<?php echo base_url(); ?>Tablas/inventario/";

        table = $tabla_inventario.DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "ajax": {
                "url": url + sucursal,
                "type": "POST"
            },
            "columns": [
                null, {
                    className: 'text-right'
                }, {
                    className: 'text-left'
                }, {
                    className: 'text-left'
                }, {
                    className: 'text-right'
                }
            ]
        });

        $tabs.on('click', 'a', function() {
            sucursal = $(this).text();
            table.ajax.url(url + sucursal).load();
        });
    });
</script>
<style>
@font-face {
    font-family: 'myfont';
    src: url(/sgf/assets/fonts/Arciform.otf);
}
#titulo{
    font-family: myfont;
}
</style>
