<?php
foreach ( $pedidos as $row ) {
	$url = base_url() . 'Fachada_pedido/ver_pedido/' . $row->id;
	
	$cliente = $row->cliente->get();
	
	$id = $row->id;
	$fecha = $row->fecha_hora;
	$dni = $cliente->dni;
	$apellido_nombre = $cliente->apellido . " " . $cliente->nombre;
	$importe = $row->importe;
?>
	<tr>
	<td><?php echo $id; ?></td>
	<td class="text-left"><?php echo $fecha; ?></td>
	<td class="text-left"><?php echo $dni . '	' . $apellido_nombre?></td>
	<td class="text-right">$<?php echo $importe; ?></td>
	<td>
		<a role="button" id="ver_pedido"class="btn btn-info pull-right" 
			href="<?php echo $url; ?>"><i class="glyphicon glyphicon-eye-open">
</i>  Ver Pedido</a>
	</td>
	</tr>
<?php
}
?>