<?php
foreach ( $linea_pedido as $row ) {
	$cantidad = $row->cantidad;
	$precio = $row->precio_producto;
	bcscale(2);
	$subtotal = bcmul($cantidad, $precio, 2);
	$nombre=$row->producto->get()->nombre_producto;
?>
	<tr>
		<td class="text-left"><?php echo $nombre; ?></td>
		<td class="text-right">$<?php echo $precio; ?></td>
		<td class="text-right"><?php echo $cantidad; ?></td>
		<td class="text-right">$<?php echo $subtotal; ?></td>
	</tr>';
<?php
}
?>
<tr>
	<td class="no-line"></td>
	<td class="no-line"></td>
	<td class="no-line"></td>
	<td class="text-right"><strong>$<?php echo $pedido->importe?></strong></td>
</tr>';