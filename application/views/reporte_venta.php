<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1 class="text-center">Ventas</h1>
			</div>
			<form id="form">
				<div class="row">
					<div class="form-group col-sm-2">
						<input type="text" class="form-control" id="input-dni"
							placeholder="DNI Cliente">
					</div>
					<div class="form-group col-sm-2">
						<input type="text" class="form-control" id="input-sucursal"
							placeholder="Sucursal">
					</div>
					<div class="form-group col-sm-2">
						<input type="date" class="form-control" id="input-min-fecha"
							placeholder="Fecha Mín">
					</div>
					<div class="form-group col-sm-2">
						<input type="date" class="form-control" id="input-max-fecha"
							placeholder="Fecha Max">
					</div>
					<button id="submit" type="submit" class="btn btn-primary col-sm-2">Filtrar</button>
				</div>
			</form>
			<table class="table" id="tablaVentas">
				<thead>
					<tr>
						<th><strong>Pedido</strong></th>
						<th class="text-center"><strong>DNI Cliente</strong></th>
						<th class="text-center"><strong>Sucursal</strong></th>
						<th class="text-center"><strong>Fecha/Hora Venta</strong></th>
						<th class="text-right"><strong>Importe</strong></th>
					</tr>
				</thead>
				<tbody id="myID">
				</tbody>
			</table>
			<!-- Panel Body -->
		</div>
		<!-- panel panel-default -->
	</div>
	<!-- col-md-9 -->
</div>

<script type="text/javascript">
$(document).ready(function() {

	var url = "<?php echo base_url(); ?>Tablas/ventas";
	dni = "";
	sucursal = "";
	minfecha = "";
	maxfecha = "";

	table = $('#tablaVentas').DataTable({
		"processing": true,
		"serverSide": true,
		"searching": false,
		"pageLength": 20,
		"lengthChange": false,
		"ajax": {
			"url": url,
			"type": "POST",
			"data": function(d) {
				d.dni = dni;
				d.sucursal = sucursal;
				d.minfecha = minfecha;
				d.maxfecha = maxfecha;
			}
		},
		"columns" : [
			null,
			{ className: 'text-center' },
			{ className: 'text-center' },
			{ className: 'text-center' },
			{ className: 'text-right' },
		],
		buttons: [
			'copy',
			'pdfHtml5',
			'csvHtml5',
			'excelHtml5',
			'print'
		],
		dom: '<"col-sm-6"B><"col-sm-6"p>rti',
	});

	$('#form').on('click', '#submit', function(event) {
		event.preventDefault();

		dni = $('#input-dni').val();
		sucursal = $('#input-sucursal').val();
		minfecha = $('#input-min-fecha').val();
		maxfecha = $('#input-max-fecha').val();
		table.ajax.reload();

		console.log(dni);
		console.log(sucursal);
		console.log(minfecha);
		console.log(maxfecha);
	});
});
</script>