<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
<script src="/proyecto/assets/js/bootstrap.min.js"></script>
<!-- <script src="/proyecto/assets/jquery/jquery.js"></script> -->
<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
-->
<script
	src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');
</script>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			<ul class="nav nav-pills nav-stacked">
				<div class="dropdown">
					<button
						class="btn btn-default dropdown-toggle btn-lg btn-block
				  "
						type="button" data-toggle="dropdown">
						Arreglos <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#">HTML</a></li>
						<li><a href="#">CSS</a></li>
						<li><a href="#">JavaScript</a></li>
					</ul>
				</div>
				<div class="dropdown">
					<button
						class="btn btn-default dropdown-toggle btn-lg btn-block
				  "
						type="button" data-toggle="dropdown">
						Catálogos <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#">HTML</a></li>
						<li><a href="#">CSS</a></li>
						<li><a href="#">JavaScript</a></li>
					</ul>
				</div>
				<div class="dropdown">
					<button
						class="btn btn-default dropdown-toggle btn-lg btn-block
				  "
						type="button" data-toggle="dropdown">
						Clientes <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#">HTML</a></li>
						<li><a href="#">CSS</a></li>
						<li><a href="#">JavaScript</a></li>
					</ul>
				</div>
				<div class="dropdown">
					<button
						class="btn btn-default dropdown-toggle btn-lg btn-block
				  "
						type="button" data-toggle="dropdown">
						Contratos <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#">HTML</a></li>
						<li><a href="#">CSS</a></li>
						<li><a href="#">JavaScript</a></li>
					</ul>
				</div>
				<div class="text-center">
					<div class="dropdown">
						<button
							class="btn btn-default dropdown-toggle btn-lg btn-block
					  "
							type="button" data-toggle="dropdown">
							Cuentas <br> Corrientes <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#">HTML</a></li>
							<li><a href="#">CSS</a></li>
							<li><a href="#">JavaScript</a></li>
						</ul>
					</div>
				</div>
				<div class="dropdown">
					<button
						class="btn btn-default dropdown-toggle btn-lg btn-block
				  "
						type="button" data-toggle="dropdown">
						Ofertas <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#">HTML</a></li>
						<li><a href="#">CSS</a></li>
						<li><a href="#">JavaScript</a></li>
					</ul>
				</div>
				<div class="dropdown">
					<button
						class="btn btn-default dropdown-toggle btn-lg btn-block
				  "
						type="button" data-toggle="dropdown">
						Productos <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#">HTML</a></li>
						<li><a href="#">CSS</a></li>
						<li><a href="#">JavaScript</a></li>
					</ul>
				</div>
				<div class="dropdown">
					<button
						class="btn btn-default dropdown-toggle btn-lg btn-block
				  "
						type="button" data-toggle="dropdown">
						Reportes <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#">HTML</a></li>
						<li><a href="#">CSS</a></li>
						<li><a href="#">JavaScript</a></li>
					</ul>
				</div>
			</ul>
		</div>
		<div class="col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Consultar Pedidos</h3>
				</div>
				<div class="panel-body">
					<form class="form-inline" role="form">
						<div class="form-group">
							<label class="control-label col-xs-5" for="text1">Dni del Cliente:</label>
							<div class="col-xs-2">
  								<input type="text" class="form-control" name="dni" id="text1">
  							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-2">
  								<button type="submit" class="btn btn-default">Consultar</button>
  							</div>
						</div>
					</form>
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Número de Pedido</th>
								<th>Fecha y Hora</th>
								<th>Nombre y Apellido</th>
								<th>Importe Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>24/09/2015 19:30</td>
								<td>Santiago Santana</td>
								<td>$ 350</td>
							</tr>
							<tr>
								<td>2</td>
								<td>24/09/2015 19:30</td>
								<td>Santiago Santana</td>
								<td>$ 350</td>
							</tr>
							<tr>
								<td>3</td>
								<td>24/09/2015 19:30</td>
								<td>Santiago Santana</td>
								<td>$ 350</td>
							</tr>
							<tr>
								<td>4</td>
								<td>24/09/2015 19:30</td>
								<td>Santiago Santana</td>
								<td>$ 350</td>
							</tr>
							<tr>
								<td>5</td>
								<td>24/09/2015 19:30</td>
								<td>Santiago Santana</td>
								<td>$ 350</td>
							</tr>
						</tbody>
					</table>
					<input type="submit" class="btn btn-info pull-right" value="Ver Pedido">
				</div>
				<!-- Panel Body -->
			</div>
			<!-- panel panel-default -->
		</div>
		<!-- col-md-9 -->
	</div>
</div>

