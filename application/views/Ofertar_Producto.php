<div class="container-fluid">
	<div class="row">
		<div class="col-md-10">
			<div class="tabbable">
				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a href="#suc-1" data-toggle="tab">Sucursal 1</a></li>
					<li><a href="#suc-2" data-toggle="tab">Sucursal 2</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="suc-1">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Productos sin Ofertar</h3>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail Second"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<label>Fecha de Fin de Oferta:</label> 
														<input type="date" name="fecha"><br>	
														<label>Descuento:</label><br>
														<input type="number" name="descuento">
														<a class="btn btn-primary pull-right"
															href="#"> <span class="glyphicon glyphicon-plus-sign"></span>
															Ofertar
														</a>
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail Second"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<label>Fecha de Fin de Oferta:</label> 
														<input type="date" name="fecha"><br>	
														<label>Descuento:</label><br>
														<input type="number" name="descuento">
														<a class="btn btn-primary pull-right"
															href="#"> <span class="glyphicon glyphicon-plus-sign"></span>
															Ofertar
														</a>
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail Second"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<label>Fecha de Fin de Oferta:</label> 
														<input type="date" name="fecha"><br>	
														<label>Descuento:</label><br>
														<input type="number" name="descuento">
														<a class="btn btn-primary pull-right"
															href="#"> <span class="glyphicon glyphicon-plus-sign"></span>
															Ofertar
														</a>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="text-center">
										<ul class="pagination">
											<li><a href="#">&laquo;</a></li>
											<li><a href="#">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#">4</a></li>
											<li><a href="#">5</a></li>
											<li><a href="#">&raquo;</a></li>
										</ul>
									</div>
								</div>
								<!-- Panel Body -->
							</div>
							<!-- panel panel-default -->
						</div>
						<!-- col-md-8 -->
					</div>
					<!-- Suc-1 -->
					<div class="tab-pane" id="suc-2">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Productos sin Ofertar</h3>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail First"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<label>Fecha de Fin de Oferta:</label> 
														<input type="date" name="fecha"><br>	
														<label>Descuento:</label><br>
														<input type="number" name="descuento">
														<a class="btn btn-primary pull-right"
															href="#"> <span class="glyphicon glyphicon-plus-sign"></span>
															Ofertar
														</a>
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail Second"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<label>Fecha de Fin de Oferta:</label> 
														<input type="date" name="fecha"><br>	
														<label>Descuento:</label><br>
														<input type="number" name="descuento">
														<a class="btn btn-primary pull-right"
															href="#"> <span class="glyphicon glyphicon-plus-sign"></span>
															Ofertar
														</a>
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail Second"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<label>Fecha de Fin de Oferta:</label> 
														<input type="date" name="fecha"><br>	
														<label>Descuento:</label><br>
														<input type="number" name="descuento">
														<a class="btn btn-primary pull-right"
															href="#"> <span class="glyphicon glyphicon-plus-sign"></span>
															Ofertar
														</a>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="text-center">
										<ul class="pagination">
											<li><a href="#">&laquo;</a></li>
											<li><a href="#">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#">4</a></li>
											<li><a href="#">5</a></li>
											<li><a href="#">&raquo;</a></li>
										</ul>
									</div>
								</div>
								<!-- Panel Body -->
							</div>
							<!-- panel panel-default -->
						</div>
						<!-- col-md-8 -->
					</div>
					<!-- Suc 2 -->
				</div>
			</div>
		</div>
	</div>
</div>
