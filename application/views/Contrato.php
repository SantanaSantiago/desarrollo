<div class="container-fluid">
	<div class="row">
		<div class="col-md-10">
			<div class="tabbable">
				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a href="#suc-1" data-toggle="tab">Sucursal 1</a></li>
					<li><a href="#suc-2" data-toggle="tab">Sucursal 2</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="suc-1">
						<div class="col-md-8">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Seleccione los productos para el contrato</h3>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail Second"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<label for="cant1">Cantidad:</label> <input type="number"
															name="cantidad" id="cant1"> <a class="btn btn-primary"
															href="#"> <span class="glyphicon glyphicon-plus-sign"></span>
															Agregar
														</a>
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail Second"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<label for="cant1">Cantidad:</label> <input type="number"
															name="cantidad" id="cant1"> <a class="btn btn-primary"
															href="#"> <span class="glyphicon glyphicon-plus-sign"></span>
															Agregar
														</a>
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail Second"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<label for="cant1">Cantidad:</label> <input type="number"
															name="cantidad" id="cant1"> <a class="btn btn-primary"
															href="#"> <span class="glyphicon glyphicon-plus-sign"></span>
															Agregar
														</a>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="text-center">
										<ul class="pagination">
											<li><a href="#">&laquo;</a></li>
											<li><a href="#">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#">4</a></li>
											<li><a href="#">5</a></li>
											<li><a href="#">&raquo;</a></li>
										</ul>
									</div>
								</div>
								<!-- Panel Body -->
							</div>
							<!-- panel panel-default -->
						</div>
						<!-- col-md-8 -->
						<div class="col-md-4">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title-center text-center ">Contrato</h3>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table
											class="table 
			                                  table-striped table-hover table-condensed">
											<thead>
												<tr>
													<th width="25%">Producto</th>
													<th width="25%">Cantidad</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Margarita</td>
													<td>50</td>
												</tr>
												<tr>
													<td>Tulipan</td>
													<td>30</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td></td>
													<td></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="panel panel-default">
										<div class="panel-body">
											<a href="<?php echo base_url(); ?>index.php/Generar_Contrato"><button type="button" class="btn btn-default">Contratar</button></a>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-body">
											<button type="button" class="btn btn-success btn-block">¡Crea
												tu propio Arreglo!</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Suc-1 -->
					<div class="tab-pane" id="suc-2">
						<div class="col-md-8">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Catálogo</h3>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail First"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<a class="btn btn-primary" href="#">Comprar</a> <a
															class="btn" href="#">Comprar</a>
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="thumbnail">
												<img alt="Bootstrap Thumbnail Second"
													src="/proyecto/assets/imagenes/Tulips.jpg" />
												<div class="caption">
													<h3>Tulipanes</h3>
													<p>
														<a class="btn btn-primary" href="#">Comprar</a> <a
															class="btn" href="#">Comprar</a>
													</p>
												</div>
											</div>
										</div>

									</div>
									<div class="text-center">
										<ul class="pagination">
											<li><a href="#">&laquo;</a></li>
											<li><a href="#">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#">4</a></li>
											<li><a href="#">5</a></li>
											<li><a href="#">&raquo;</a></li>
										</ul>
									</div>
								</div>
								<!-- Panel Body -->
							</div>
							<!-- panel panel-default -->
						</div>
						<!-- col-md-8 -->
						<div class="col-md-4">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Contrato</h3>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table
											class="table 
			    table-striped table-hover table-condensed">
											<thead>
												<tr>
													<th width="25%">Producto</th>
													<th width="25%">Cantidad</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>TB - Monthly</td>
												</tr>
												<tr class="active">
													<td>1</td>
													<td>TB - Monthly</td>
												</tr>
												<tr class="success">
													<td>2</td>
													<td>TB - Monthly</td>
												</tr>
												<tr class="warning">
													<td>3</td>
													<td>TB - Monthly</td>
													<td>03/04/2012</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="panel panel-default">
										<div class="panel-body">
											<button type="button" class="btn btn-default">Contratar</button>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-body">
											<button type="button" class="btn btn-success btn-block">¡Crea
												tu propio Arreglo!</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Col md-4 -->
					</div>
					<!-- Suc 2 -->
				</div>
			</div>
		</div>
	</div>
</div>