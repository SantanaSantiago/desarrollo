<form id="formModificarCliente" class="form-horizontal" role="form">
	<div class="form-group">
		<label class="control-label col-sm-3" for="dni">DNI:</label>
		<div class="col-xs-5">
			<input value="<?php echo $dni; ?>" class="form-control"
				name="dni" id="dni" required="required" disabled />
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-xs-3" for="nombre">Nombre:</label>
		<div class="col-xs-5">
			<input value="<?php echo $nombre; ?>" type="text"
				class="form-control" name="nombre" id="nombre" required="required" />
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-xs-3" for="apellido">Apellido:</label>
		<div class="col-xs-5">
			<input value="<?php echo $apellido; ?>" type="text"
				class="form-control" name="apellido" id="apellido"
				required="required" />
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-xs-3" for="email">Email:</label>
		<div class="col-xs-5">
			<input value="<?php echo $email; ?>" type="email"
				class="form-control" name="apellido" id="email" required="required" />
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-xs-3" for="direccion">Dirección:</label>
		<div class="col-xs-5">
			<input value="<?php echo $direccion; ?>" type="text"
				class="form-control" name="direccion" id="direccion"
				required="required" />
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-xs-3" for="telefono">Teléfono:</label>
		<div class="col-xs-5">
			<input value="<?php echo $telefono; ?>" type="text"
				class="form-control" name="telefono" id="telefono"
				required="required" />
		</div>
	</div>
	<div class="form-group text-right">
		<span class="col-lg-10">
			<button class="btn btn-primary" data-dismiss="modal">Cancelar</button>
			<button id="registrar" class="btn btn-danger"
			type="submit" name="op">Modificar</button>
		</span>
	</div>
</form>

<script>

$( document ).ready(function() {
	$("#txtTelefonoCliente").mask('(0000) 000-0000');
	$("#numDniCliente").mask('00000000');
// 	$("#txtNombreCliente").mask('00000000000000000');
	jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});

	$('#formModificarCliente').validate({
		rules:{
			nombre:{
				required:true,
				lettersonly: true,
				minlength:3,
				maxlength:41
				},
			email:{
				required:true
			},
			apellido:{
				required:true,
				lettersonly: true,
				minlength:3,
				maxlength:41
				},
			dni:{
				minlength:7,
				maxlength:9,
				required:true
				},
 			telefono:{
				required:true,
 	 			}
			},
		messages: {
			dni:"Ingrese número de DNI",
			nombre: "Ingrese un nombre",
			apellido: "Ingrese un apellido",
			email:"Ingrese una dirección email correcta",
			telefono:"Ingrese un número de teléfono"
			}
		});

});
</script>
<style>
 .form-group .error {
            color: red;
        }

.form-group .valid {
            color: green;
        }

</style>

