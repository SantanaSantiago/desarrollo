<?php
	$date = strftime("%Y-%m-%d", strtotime("04/22/2016"));
?>
<form id="formRegistrarIngreso" class="form-horizontal" role="form">
	<div class="form-group">
		<label class="control-label col-xs-3" for="stock">Stock:</label>
		<div class="col-xs-5">
			<input title="Debe ingresar el stock" type="number"
				class="form-control" name="stock" id="stock" required="required" />
		</div>
	</div>
	<div id="groupVencimiento" class="form-group">
		<label class="control-label col-xs-3" for="vencimiento"> Fecha de
			Vencimiento:</label>
		<div id="fecha_bonita" class="col-xs-5">
			<fieldset>
				<input title="Debe ingresar el vencimiento" type="date"
					class="form-control" name="fecha" id="vencimiento" min="<?php echo $date; ?>"
					required="required" />
			</fieldset>
		</div>
	</div>
	<div class="form-group text-right">
		<span class="col-lg-10"> <button class="btn btn-danger"
			data-dismiss="modal">Cancelar</button>
			<button class="btn btn-primary" type="submit" name="op">Registrar</button>
		</span>
	</div>
</form>
<script>
$(document).ready(function() {
	$('#groupVencimiento').hide();

	if (tipo_producto.text() == "Flor Natural"){
		$('#fecha_bonita').updatePolyfill();
		$('#groupVencimiento').show();
	}
	$('#formRegistrarIngreso').validate({
		rules:{
			stock:{
				required:true,
				digits:true
				},
			},
		messages: {
			stock: "Ingrese la cantidad de stock",
			}
		});
});
</script>
<style>
.form-group .error {
	color: red;
}

.form-group .valid {
	color: green;
}
</style>