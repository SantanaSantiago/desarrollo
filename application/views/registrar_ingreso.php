<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2 class="text-center"><span class="glyphicon glyphicon-tree-deciduous"></span> Productos</h2>
			</div>
			<table class="table table-striped table-bordered dt-responsive" id="tablaInventario">
				<thead>
					<tr>
						<th><strong>Nombre</strong></th>
						<th class="text-center"><strong>Tipo de Producto</strong></th>
						<th class="text-center"><strong>Descripción</strong></th>
						<th class="text-right"><strong></strong></th>
					</tr>
				</thead>
				<tbody id="myID">
					</tbody>
			</table>
		</div>
	</div>
</div>
<div>
<?php $this->load->view('componentes/form_modal');?>
</div>
<script type="text/javascript">
	var nombre_producto;
	var tipo_producto;

	$(document).ready(function() {
		var $modal = $('#ventanaModal');
		var $my_id = $("#myID");
		var $tabs = $('#tabs');
		
		var sucursal = $tabs.find('.active').text();
		var url = "<?php echo base_url(); ?>Tablas/productos/";
		
		table = $('#tablaInventario').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": url + sucursal,
				"type": "POST"
			},
			
			"columns" : [
				null,
				{ className: 'text-center' },
				{ className: 'text-center' },
				{ className: 'text-center', 'orderable': false, 'searchable': false}
			],
			

		});
		
		$tabs.on('click', 'a', function() {
			sucursal = $(this).text();
			table.ajax.url(url + sucursal).load();
		});
		
		$my_id.on('click','#registrar', function(event) {
			event.preventDefault();
			var linea = $(this).parent().parent();
			var nombre = linea.children(":first");
			nombre_producto = nombre.text();
			tipo_producto = nombre.next(); 
		});	


		$modal.on('show.bs.modal', function(event){
			$modal.find('.modal-title').text("Formulario de Ingreso del producto " + nombre_producto);
 			$modal.find('.modal-body').load('<?php echo base_url(); ?>Fachada_producto/formulario_ingreso/' + sucursal);
  			$modal.find('.modal-footer').hide();
		});

		$modal.on('submit', '#formRegistrarIngreso', function(event) {
			event.preventDefault();
			console.log("esto no funciona");

			var vencimiento = $('#vencimiento').val();
			var stock = $('#stock').val();
			
			$.post('<?php echo base_url(); ?>Fachada_producto/registrar_ingreso_producto/', 
				{	producto: nombre_producto, 
					stock: stock, 
					vencimiento: vencimiento,
					sucursal: sucursal	},
				function(resultado) {
					console.log("registrar ingreso producto");
					
					if (resultado == "si"){			
						$modal.find('.modal-title').text("Mensaje de Información");
						$modal.find('.modal-body').text("La transacción ha sido exitosa");
						$modal.find('.modal-footer').show();
						$modal.find('.modal-footer>#cancelar').hide();
					} else {
						$modal.find('.modal-title').text("Mensaje de Información");
						$modal.find('.modal-body').text("Ha ocurrido un error durante la transacción");
						$modal.find('.modal-footer').show();
						$modal.find('.modal-footer>#cancelar').hide();
					}
				}
			);
		});
	});
</script>