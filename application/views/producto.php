<div class="panel panel-primary col-md-4" id="<?php echo $nombre_producto; ?>">
	<form id="form<?php echo $nombre_producto; ?>"
		name="form<?php echo $nombre_producto; ?>" class="myform" action="#">
		<div class="row">
			<div id="quedan-<?php echo $nombre_producto; ?>"
				class="alert alert-danger col-xs-offset-2 col-xs-8" role="alert">
				<span class="sr-only">Error: </span><strong>
        ¡Quedan <?php echo $stock; ?> unidades!</strong>
      </div>
		</div>
		<h3 class="text-center"><?php echo $nombre_producto; ?></h3>
		<div class="thumbnail" id="thumb-<?php echo $nombre_producto; ?>">
			<img src="<?php echo base_url() . 'assets/imagenes/' . $path_img; ?>"
				style="width: 300px; height: 300px" />
		</div>
		<h3 id="precio-<?php echo $nombre_producto; ?>" class="text-right">
			<strong>$<?php echo $precio; ?></strong>
		</h3>
		<div class="form-group">
		<label for='input-<?php echo $nombre_producto; ?>'>Cantidad:</label> <input
			id="input-<?php echo $nombre_producto; ?>"
			alt="<?php echo $nombre_producto; ?>" class="form-control"
			type="number" name="cantidad" min="1" max="<?php echo $stock; ?>"
			step="1" />
		</div>
		<span class="row">
		<button class="btn btn-primary" name="agregar"
			id="agregar-<?php echo $nombre_producto; ?>">
			<span class="glyphicon glyphicon-plus-sign"></span>Agregar
		</button>
		</span>
	</form>
</div>
