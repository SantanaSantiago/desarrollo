<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="invoice-title">
				<div class="row">
					<br>
					<input class="pull-right" type="button" name="imprimir" value="Imprimir Factura" onclick="window.print();">
				</div>
				<h2>Factura</h2><h3 class="pull-right">Pedido <?php echo $pedido->id;?></h3>
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-6">
					<address>
					<strong>Entregado por:</strong><br>
						<?php echo $pedido->sucursal->get()->nombre_sucursal?><br>
						<?php echo $pedido->sucursal->get()->telefono?><br>
						<?php echo $pedido->sucursal->get()->mail?>
					</address>
				</div>
				<div class="col-xs-6 text-right">
					<address>
					<strong>Entregado a:</strong><br>
						<?php echo $pedido->cliente->get()->nombre." ".$pedido->cliente->get()->apellido?><br>
						<?php echo $pedido->cliente->get()->direccion?><br>
						<?php echo $pedido->cliente->get()->email?><br>
					</address>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<address>
						<strong>Método de Pago:</strong><br>
						Efectivo<br>
					</address>
				</div>
				<div class="col-xs-6 text-right">
					<address>
						<strong>Fecha y Hora de Facturación:</strong><br>
						<?php
							setlocale(LC_TIME, "es_AR");
							date_default_timezone_set('America/Argentina/Buenos_Aires');
							$date = new DateTime();
							echo $date->format('Y-m-d H:i:s');
						?>
						<br/>
					</address>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><strong>Detalle</strong></h3>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-condensed">
							<thead>
								 <tr>
									<td><strong>Producto</strong></td>
									<td class="text-center"><strong>Tipo Producto</strong></td>
									<td class="text-right"><strong>Precio Unitario</strong></td>
									<td class="text-right"><strong>Cantidad</strong></td>
									<td class="text-right"><strong>Importe</strong></td>
								</tr>
							</thead>
							<tbody id="cuerpo_tabla">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
$(document).ready( function () {
	var pedido = <?php echo $pedido->id; ?>;
	function cargar_tabla(result) {
		$('#cuerpo_tabla').html(result);	
	};
		
	$.post("<?php echo base_url(); ?>Factura/detalle_factura",
		{id_pedido: pedido}, 
		function(result) { 
			cargar_tabla(result); 
		}
	);
	
	
});
</script>