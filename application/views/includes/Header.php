<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
       
<?php header('Content-Type: text/html; charset=utf-8'); ?>

<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/jquery.bootstrap-touchspin.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/datatables/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/datatables/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/datatables/responsive.dataTables.min.css" rel="stylesheet">

<script src="<?php echo base_url(); ?>assets/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/mask.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.print.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/pdfmake/pdfmake.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/pdfmake/vfs_fonts.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jszip.min.js" type="text/javascript"></script>


<!-- <script	src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script> -->
<!-- polyfiller file to detect and load polyfills -->
<script src="<?php echo base_url(); ?>assets/js/polyfiller.js" type="text/javascript"></script>
<script>
	webshim.setOptions('basePath', '<?php echo base_url(); ?>assets/js/shims/');
	webshims.setOptions('waitReady', false);
	webshims.setOptions('forms-ext', {types: 'date'});
	webshims.polyfill('forms forms-ext');
</script>
</head>
<body>