<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2 id="titulo" class="text-center"><span class="glyphicon glyphicon-user"></span> Clientes</h2>
			</div>
			<table class="table  dt-responsive" id="tablaInventario">
				<thead>
					<tr>
						<th><strong>DNI</strong></th>
						<th><strong>Nombre</strong></th>
						<th><strong>Apellido</strong></th>
						<th><strong>Email</strong></th>
						<th><strong>Dirección</strong></th>
						<th><strong>Teléfono</strong></th>
						<th><strong></strong></th>
					</tr>
				</thead>
				<tbody id="myID">
				</tbody>
			</table>
		</div>
	</div>
</div>
<div>
<?php $this->load->view('componentes/form_modal');?>
</div>

<script type="text/javascript">
var dni; 
var nombre;
var apellido;
var email;
var direccion;
var telefono;

$(document).ready(function() {
		var dni;
		var url = "<?php echo base_url(); ?>Tablas/clientes/modificar";
		var $modal = $('#ventanaModal');
		var $myID = $('#myID');

		table = $('#tablaInventario').DataTable({
			"processing": true,
			"serverSide": true,
			"responsive": true,
			"ajax": {
				"url": url,
				"type": "POST"
			},
			"columns" : [
				null,
				null,
				null,
				null,
				null,
				null,
				{ "orderable": false, "searchable": false}
			]
		});

		$myID.on('click','.btn-warning', function(event) {
			event.preventDefault();
			dni = $(this).attr('id');
		});	


		$modal.on('show.bs.modal', function(event){
			$modal.find('.modal-title').text("Modificación del Cliente");
 			$modal.find('.modal-body').load('<?php echo base_url(); ?>Fachada_cliente/Formulario_modificacion/' + dni);
  			$modal.find('.modal-footer').hide();
		});
		
		$modal.on('submit', '#formModificarCliente', function(event) {
			event.preventDefault();

			var dni =$('#dni').val();
			var nombre =  $('#nombre').val();
			var apellido = $('#apellido').val();
			var email = $('#email').val();
			var direccion= $('#direccion').val();
			var telefono= $('#telefono').val();
	
			console.log("dni: " + dni +
						" nombre: " + nombre +
						" apellido: " + apellido +
						" email: " + email +
						" direccion: " + direccion +
						" telefono: " + telefono );
			
			$.post('<?php echo base_url();?>Fachada_cliente/guardar_modificacion/', 
				{	dni: dni,
					nombre: nombre,
					apellido: apellido,
					email: email,
					direccion: direccion,
					telefono: telefono	}, 
					
				function(resultado) {
					console.log("guardar_modificación");
					$modal.find('.modal-title').text("Mensaje de Información");

					if (resultado == "si") {
						$modal.find('.modal-body').text("La transacción ha sido exitosa");
						$modal.find('.modal-footer').show();
						$modal.find('.modal-footer>#cancelar').hide();
						table.ajax.reload();
					} else {
						$modal.find('.modal-body').text("Ha ocurrido un error durante la transacción");
						$modal.find('.modal-footer').show();
						$modal.find('.modal-footer>#cancelar').hide();
					}
				}
			);
		});
});

</script>
<style>
@font-face {
    font-family: 'myfont';
    src: url(/sgf/assets/fonts/Arciform.otf);
}
#titulo{
font-family: myfont;
}

</style>