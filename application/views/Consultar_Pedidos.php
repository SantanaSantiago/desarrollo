<div class="container-fluid">
	<div class="page-header">
		<h2 id="titulo" class="text-center"><span class="glyphicon glyphicon-list-alt"></span> Pedidos</h2>
	</div>
	<table class="table table-hover dt-responsive" id="tablaPedidos">
		<thead>
			<tr>
				<th>Número de Pedido</th>
				<th class="text-left">Fecha y Hora</th>
				<th class="text-left">Cliente</th>
				<th class="text-right"><strong>Importe Total</strong></th>
				<th class="text-right"><strong></strong></th>
			</tr>
		</thead>
		<tbody id="cuerpoTabla">
		</tbody>
	</table>
</div>


<script type="text/javascript">
$(document).ready( function () {
	var sucursal = $('#tabs > .active').text();

	function cargar_tabla(result) {
		$('#cuerpoTabla').html(result);
		table = $('#tablaPedidos').DataTable({
			"responsive":		true,
			"retrieve":			true,
			"paging":			true
		});
	};
		
	$('#tabs').on('click', 'a', (function(){
		sucursal = $(this).text();
		$.post("<?php echo base_url() ?>Fachada_pedido/mostrar_pedidos/",
				{sucursal: sucursal}, 
				function(result) { 
					console.log(sucursal);
					cargar_tabla(result); 
				});	
		})
	);
	
	$.post("<?php echo base_url() ?>Fachada_pedido/mostrar_pedidos/",
			{sucursal:sucursal}, 
			function(result) { 
				cargar_tabla(result); 
			}
	);	
});
</script>
<style>
@font-face {
    font-family: 'myfont';
    src: url(/sgf/assets/fonts/Arciform.otf);
}
#titulo{
font-family: myfont;
}

</style>