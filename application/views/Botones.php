<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-pills nav-justified">
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" type="button" role="button"> Clientes <span
					class="caret"></span>
			</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo base_url(); ?>Fachada_cliente/listar">Buscar</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Fachada_cliente/crearCliente">Crear</a></li>
					<li><a href="<?php echo base_url(); ?>Fachada_cliente/modificar_cliente">Modificar Datos</a></li>
					<li><a href="<?php echo base_url(); ?>Fachada_cliente/dar_baja_cliente">Dar de Baja</a></li>
				</ul></li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" type="button"
				role="button"> Ofertas <span class="caret"></span>
			</a>
				<ul class="dropdown-menu">
					<li><a href="#">Consultar</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Ofertar_Producto">Crear</a></li>
					<li><a href="#">Eliminar</a></li>
					<li><a href="#">Enviar por correo electrónico</a></li>
				</ul></li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" type="button"
				role="button"> Productos <span class="caret"></span>
			</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo base_url(); ?>index.php/Fachada_producto/consultar_inventario">Consultar Inventario</a></li>
					<li><a
						href="<?php echo base_url(); ?>index.php/Fachada_producto/alta_producto">Crear</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/CrearArreglo">Crear
							Arreglo</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Fachada_producto/registrar_ingreso">Registrar Ingreso</a></li>
					<li><a href="#">Cambiar Estado</a></li>
					<li><a href="#">Dar de Baja</a></li>
				</ul></li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" type="button"
				role="button"> Cuentas Corrientes <span class="caret"></span>
			</a>
				<ul class="dropdown-menu">
					<li><a href="#">Consultar</a></li>
					<li><a href="#">Crear</a></li>
					<li><a href="#">Dar de Baja</a></li>
				</ul></li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" type="button"
				role="button"> Reportes <span class="caret"></span>
			</a>
				<ul class="dropdown-menu">
					<li><a
						href="<?php echo base_url(); ?>index.php/Fachada_reporte/reporte_venta"">Ventas</a></li>
					<li><a href="#">Flores Naturales</a></li>
				</ul>
			</li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" type="button"
				role="button"> Nueva Venta <span class="caret"></span>
			</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo base_url(); ?>index.php/Fachada_pedido/consultar_pedidos">Consultar Pedidos</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Fachada_pedido/realizar_pedido">Realizar Pedido</a></li>
				</ul></li>
		</ul>
	</div>
</div>