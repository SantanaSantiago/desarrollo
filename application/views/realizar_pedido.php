<div class="container-fluid">
	<div class="row"><div class="panel panel-default"></div></div>
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 id="titulo" class="panel-title-center text-center">Catálogo</h3>
				</div>
				<div class="panel-body">
					<div class="row" id="contenedor_productos"></div>
					<div class="row">
						<div class="text-center">
							<ul class="pagination">
								<li><a href="#">&laquo;</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">&raquo;</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Panel Body -->
			</div>
			<!-- panel panel-default -->
		</div>
		<!-- col-md-8 -->
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 id="titulo" class="panel-title-center text-center "><img class="carrito" src="<?php echo base_url(); ?>assets/iconos/car.png" alt="x" />Carrito de Compra</h3>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table
							class="table table-striped table-hover table-condensed">
							<thead>
								<tr>
									<th width="25%" class="text-left"><strong>Producto</strong></th>
									<th width="25%" class="text-right"><strong>Cantidad</strong></th>
									<th width="25%" class="text-right"><strong>Importe</strong></th>
									<th width="25%" class="text-center"><strong></strong></th>
								</tr>
							</thead>
							<tbody id="contenedor_carrito">
							</tbody>
						</table>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<button type="button" class="btn btn-default" id="comprar" disabled="disabled">Comprar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/js/realizar_pedido.js" type="text/javascript"></script>
<style>
img.carrito{
 width: 50px; height: 50px;
}

.panel-info {
  border-radius: 0;
  border-color: #1A4969;
}

.panel-info > .panel-heading, .btn-danger {
  background: rgba(58, 60, 48, 0.74);
  color: #E3DDAF;
  border-color: rgba(58, 60, 48, 0.74);
}

.panel-primary {
	border-color: #FFFFFF #FFFFFF #E3DDAF #FFFFFF;
}

form .error {
	color: red;
}

form .valid {
	color: green;
}

.btn {
  border-radius: 0;
}

.btn-primary {
  background: rgba(58, 60, 48, 0.74);
  color: #E3DDAF;
  border-color: rgba(58, 60, 48, 0.74);
}

.btn-primary:hover{
  background: rgba(58, 60, 48, 0.88);
  color: #E3DDAF;
  border-color: rgba(58, 60, 48, 0.74);
}
.btn-primary:active, .btn-primary.active, .btn-primary:focus {
  background: rgba(58, 60, 48, 0.74);
  color: #E3DDAF;
  border-color: rgba(58, 60, 48, 0.74);
}

.alert {
	text-align: center;
	background: rgba(132, 132, 132, 0.42);
	color: black;
	border-color: rgba(132, 132, 132, 0.42);
	opacity: 0.9;
	font-size: 1em;
}

@font-face {
    font-family: 'myfont';
    src: url(/sgf/assets/fonts/Arciform.otf);
}
#titulo{
font-family: myfont;
}

</style>