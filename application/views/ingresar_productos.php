<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Inventario</h3>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-hover" id="tablaPedidos">
						<thead>
							<tr>
								<th><strong>Producto</strong></th>
								<th class="text-center"><strong>Número de Lote</strong></th>
								<th class="text-center"><strong>Fecha de Ingreso</strong></th>
								<th class="text-center"><strong>Fecha de Vencimiento</strong></th>
								<th class="text-right"><strong>Stock</strong></th>
								<th class="text-right"><strong></strong></th>
								
								
							</tr>
						</thead>
						<tbody>
						<?php
						$lote = new Lote();
						$lotes = $lote->get();
						foreach ( $lotes as $row ) {
							echo '<tr>';
					        echo '<td>' . $row->inventario->get()->producto->nombre_producto . "</td>";
					        echo '<td class="text-center">' . $row->numero_lote . "</td>";
					        echo '<td class="text-center">' . $row->fecha_ingreso . "</td>";
					        echo '<td class="text-center">' . $row->fecha_vencimiento . "</td>";
					        echo '<td class="text-right">' . $row->stock_lote . "</td>";
					        echo '</tr>';
						}
						?>
						</tbody>
					</table>
					<a class="btn btn-info pull-right" 
						href="<?php echo base_url(); ?>index.php/ver_pedido" 
						role="button" >Ver Pedido</a>
				</div>
				<!-- Panel Body -->
			</div>
			<!-- panel panel-default -->
		</div>
		<!-- col-md-9 -->
	</div>
</div>

<script type="text/javascript">

	$(document).ready( function () {
    	$('#tablaPedidos').DataTable({
        	"scrollY":        "200px",
        	"scrollCollapse": true,
        	"paging":         true
    });
} );

</script>
