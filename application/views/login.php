<link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrap">
                <p class="form-title">Login</p>
                <form class="login">
                    <input id="user" type="text" placeholder="Mail" />
                    <input id="pass" type="password" placeholder="Contraseña" />
                    <input id="submit" type="submit" value="Iniciar Sesión" class="btn btn-success btn-sm" />
                </form>
            </div>
        </div>
        <div class="alert alert-danger alert_usuario" role="alert">
            <strong>Usuario o contraseña no válido.</strong>
        </div>
        <div class="posted-by">
            Desarrollado por: Santana Santiago, Gimenez Juan Martín
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var $user = $('#user');
        var $pass = $('#pass')
        var $submit = $('#submit');
        var $alerta = $('.alert');

        $alerta.hide();

        function login(user, pass) {
            $.post("<?php echo base_url(); ?>login/autenticar", {
                    user: user,
                    pass: pass
                },
                function(rol) {
                    console.log("u: " + user + " pass: " + pass + " rol: " + rol);
                    var dir;
                    if (rol == 'cliente') {
                        dir = '<?php echo base_url(); ?>Fachada_pedido/realizar_pedido';
                        window.location.href = dir;
                    } else if (rol == 'vendedor') {
                        dir = '<?php echo base_url(); ?>Home';
                        window.location.href = dir;
                    } else if (rol == 'administrador') {
                        dir = '<?php echo base_url(); ?>Home';
                        window.location.href = dir;
                    } else {
					    $alerta.show();
                    }
                }
            );
        };
		
		$alerta.click(function(e) {
			e.preventDefault();
			$alerta.hide('slow');
		})

        $submit.click(function(e) {
            e.preventDefault();
            user = $user.val();
            pass = $pass.val();
            login(user, pass);
        });
    });
</script>
<style>
    .alert_usuario {
        position: fixed;
        width: 25%;
        top: 50%;
        left: 37%;
        margin: 0 auto;
    }
</style>