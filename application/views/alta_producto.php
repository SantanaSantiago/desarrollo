<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="col-md-12">
				<!-- panel donde ira el form de alta de cliente -->
				<div class="page-header">
					<h2 class="text-center"><span class="glyphicon glyphicon-tree-deciduous"></span> Productos</h2>
				</div>
				<div class="col-md-12">
				<?php echo form_open_multipart('Fachada_producto/guardar_producto',
						array('class'=>'form-horizontal', 'id'=>'form_alta_producto'));?>
					<div class="form-group">
						<label class="control-label col-xs-3" for="nombre">Nombre</label>
						<div class="col-xs-7">
							<input type="text" class="form-control" name="nombre" id="nombre"
								required="required">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-xs-3" for="precio">Precio</label>
						<div class="col-xs-7">
							<input type="text" class="form-control money" name="precio" id="precio"
								required="required">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-xs-3" for="descripcion">Descripción</label>
						<div class="col-xs-7">
							<input type="text" class="form-control" name="descripcion"
								id="descripcion">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-xs-3" for="tipo">Tipo</label>
						<div class="col-xs-7">
							<select class="form-control" name="tipo" id="tipo" name="tipo">
								<option value="complemento">Complemento</option>
								<option value="Flor Natural">Flor Natural</option>
								<option value="Flor Artificial">Flor Artificial</option>
							</select>
						</div>
					</div>


					<div class="form-group" enctype="multipart/form-data">
						<label class="control-label col-xs-3" for="imagen">Imagen</label>
						<input type="file" name="imagen" id="imagen" />
					</div>
					<div class="col-md-4 col-md-offset-4">
						<button class="btn btn-primary btn-block" type="submit" name="op"><i class="glyphicon glyphicon-ok"></i> Crear</button>
					</div>
				<?php form_close(); ?>
				</div>
			</div>
			<!-- Panel Body -->
		</div>
		<!-- panel panel-default -->
	</div>
</div>
<!-- fin de row principal-->
<script type="text/javascript"> 
$(document).ready(function() {
	$('.money').mask('000000,00',{reverse: true});
	$("#nombre").mask('SSSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', 
			{'translation': {
				A: {pattern: /[A-Za-z ]/},}});
	$('#form_alta_producto').validate({
		rules:{
			nombre:{
				required:true,
				minlength:3,
				maxlength:41
				},
			precio:{
				required:true,
			},
			},
		messages: {
			nombre: "Ingrese un nombre",
			precio:"Ingrese el precio del producto",
			}
		});

});
</script>
<style>
 .form-group .error {
            color: red;
        }

.form-group .valid {
            color: green;
        }
.btn {
		border-radius: 0;
	}
</style>