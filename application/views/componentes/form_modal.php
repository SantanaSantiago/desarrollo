<div id="ventanaModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button id="aceptar"class="btn btn-primary" data-dismiss="modal">Aceptar</button>
				<a id="cancelar" href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a>
			</div>
		</div>
	</div>
</div>