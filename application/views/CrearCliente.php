<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-md-12">
                <div class="page-header">
                    <h2 id="titulo" class="text-center"><span class="glyphicon glyphicon-user"></span> Clientes</h2>
                </div>
                <?php echo form_open("Fachada_cliente/guardarCliente", array('class' => 'form-horizontal', 'id' => 'formCrearCliente')); ?>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="dni">DNI:</label>
                        <div class="col-xs-7">
                            <input title="Debe ingresar el dni" type="number" class="form-control" name="dni" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="nombre">Nombre:</label>
                        <div class="col-xs-7">
                            <input type="text" class="form-control" name="nombre" id="nombre" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="apellido">Apellido:</label>
                        <div class="col-xs-7">
                            <input type="text" class="form-control" name="apellido" id="apellido">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="email">Email:</label>
                        <div class="col-xs-7">
                            <input type="email" class="form-control" name="email" id="email" placeholder="@Ingrese email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="password">Contraseña:</label>
                        <div class="col-xs-7">
                            <input type="password" class="form-control" name="password" id="password" placeholder="@Ingrese contraseña">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="confirm_password">Repetir Contraseña:</label>
                        <div class="col-xs-7">
                            <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="@Ingrese contraseña">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="direccion">Domicilio:</label>
                        <div class="col-xs-7">
                            <input type="text" class="form-control" name="direccion" placeholder="calle y número">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="telefono">Teléfono:</label>
                        <div class="col-xs-7">
                            <input type="text" id="telefono" class="form-control" placeholder="ej:(0312) 454-1233" name="telefono" id="telefono">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    <button class="btn btn-primary btn-block" type="submit" name="op"><i class="glyphicon glyphicon-ok"></i> Crear</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        $("#telefono").mask('(0000) 000-0000');
        $('[name="dni"]').mask('00000000');
        $("#nombre").mask('SSSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
            'translation': {
                A: {
                    pattern: /[A-Za-z ]/
                },
            }
        });
        $("#password").mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
        $("#apellido").mask('SSSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
            'translation': {
                A: {
                    pattern: /[A-Za-z ]/
                },
            }
        });

        $('#formCrearCliente').validate({
            rules: {
                nombre: {
                    required: true,
                    minlength: 3,
                    maxlength: 41
                },
                apellido: {
                    required: true,
                    minlength: 3,
                    maxlength: 41
                },
                email: {
                    required: true
                },
                password: {
                    minlength: 6,
                    required: true,
                    noSpace: false
                },
                confirm_password: {
                    minlength: 6,
                    required: true,
                    equalTo: "#password"
                },
                dni: {
                    minlength: 7,
                    maxlength: 9,
                    required: true
                },
                telefono: {
                    required: true,
                    minlength: 15
                },
                direccion:{
					required: true,
                    }
            },
            messages: {
                dni: "Ingrese número de DNI",
                nombre: "Ingrese nombre",
                apellido: "Ingrese apellido",
                email: "Ingrese dirección de email válida",
                password: "Ingrese contraseña. Debe contener más de 6 carácteres",
                confirm_password: "Las contraseñas no coinciden",
                telefono: "Ingrese un número de teléfono válido",
                direccion: "Ingrese domicilio"
            }
        });
    });
</script>
<style>
    .form-group .error {
        color: red;
    }
    
    .form-group .valid {
        color: green;
    }
    
    .btn {
        border-radius: 0;
    }
@font-face {
    font-family: 'myfont';
    src: url(/sgf/assets/fonts/Arciform.otf);
}
#titulo{
font-family: myfont;
}
</style>