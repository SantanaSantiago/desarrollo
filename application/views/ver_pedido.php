
<div class="page-header">
	<h2 id="titulo" class="text-center"><span class="glyphicon glyphicon-list-alt"></span> Pedido <?php echo $id_pedido; ?></h2>
</div>
<div class="col-md-3 col-md-offset"></div>
<table
	class="table table-hover dt-responsive">
	<thead>
		<tr>
			<th class="text-left"><strong>Producto</strong></th>
			<th class="text-right"><strong>Precio</strong></th>
			<th class="text-right"><strong>Cantidad</strong></th>
			<th class="text-right"><strong>Subtotal</strong></th>
		</tr>
	</thead>
	<tbody id="cuerpo_tabla">
	</tbody>
</table>
<div class="panel panel-default">
	<div class="panel-body">
				<?php
				$atts = array (
						// atributos botón
						'id' => "ventana_factura",
						'class' => "btn btn-info pull-right",
						'role' => "button",
						// atributos ventana
						'width' => '800',
						'height' => '600',
						'scrollbars' => 'yes',
						'status' => 'yes',
						'resizable' => 'yes',
						'screenx' => '0',
						'screeny' => '0' 
				);
				
				$url = base_url () . 'index.php/Factura/imprimir_factura/' . $id_pedido;
				
				echo anchor_popup ( $url, '<i class="glyphicon glyphicon-usd"></i> Pagar', $atts );
				?>
	</div>
</div>

<script type="text/javascript">
$(document).ready( function () {
	var pedido = <?php echo $id_pedido; ?>;
	var $pagar = $('#ventana_factura');

	function cargar_tabla(result) {
		$('#cuerpo_tabla').html(result);	
	};
		
	$.post("<?php echo base_url(); ?>Fachada_pedido/cargar_pedido",
		{id_pedido: pedido}, 
		function(result) { 
			cargar_tabla(result); 
		}
	);
	
	var volver_a_home = function() {
		window.location.href = '<?php echo base_url(); ?>Fachada_pedido/consultar_pedidos';
	};

	$pagar.click(function(){
		console.log("pagar");
		$.get('<?php echo base_url(); ?>Fachada_pedido/asentar_venta/' + pedido).done(
			function(result) {
				console.log(result);
				volver_a_home();
			}
		);
	});	
});
</script>
<style>
@font-face {
    font-family: 'myfont';
    src: url(/sgf/assets/fonts/Arciform.otf);
}
#titulo{
font-family: myfont;
}

</style>
