<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2 id="titulo" class="text-center dt-responsive"><span class="glyphicon glyphicon-user"></span> Clientes</h2>
			</div>
			<table class="table" id="tablaInventario">
				<thead>
					<tr>
						<th><strong>DNI</strong></th>
						<th><strong>Nombre</strong></th>
						<th><strong>Apellido</strong></th>
						<th><strong>Email</strong></th>
						<th><strong>Dirección</strong></th>
						<th><strong>Teléfono</strong></th>
						<th><strong></strong></th>
					</tr>
				</thead>
				<tbody id="myID">
				</tbody>
			</table>
		</div>
	</div>
</div>
<div id="contenedor_modal">
	<?php $this->load->view('componentes/form_modal');?>
</div>

<script type="text/javascript">
var dar_baja_cliente = (function() {
	var base_url = window.location.origin + '/sgf/';
	
	var $ventanaModal;
	var $btn_aceptar;
	var $btn_cancelar;
	var $contenedor_modal;
	var $my_id;
	
	var dni;
	var table;
	
	var inicializar = function () {
		$ventanaModal = $('#ventanaModal');
		$btn_aceptar = $('#aceptar');
		$btn_cancelar = $('#cancelar');
		$contenedor_modal = $('#contenedor_modal');
		$my_id = $('#myID');
		
		inicializar_tabla();
	};

	var inicializar_tabla = function () {
		var url = "<?php echo base_url(); ?>Tablas/clientes/eliminar";
		
		table = $('#tablaInventario').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": url,
				"type": "POST"
			},
			"rowId": 7,
			"columns" : [
				null,
				null,
				null,
				null,
				null,
				null,
				{ "orderable": false, "searchable": false}
			],
		});
	};
	
	var setearVentanaModal = function(msg_title, msg_body) {
		$ventanaModal.find('.modal-title').text(msg_title);
		$ventanaModal.find('.modal-body').text(msg_body);
	};
	
	var obtenerClienteLinea = function($this, event) {
		console.log("linea:");
		var dni = $this.attr('id');
		console.log(dni);

		row = '#row-' + dni;
		$nombre = $(row + ' :nth-child(2)');
		nombre = $nombre.text();
		console.log(nombre);
		apellido = $(row + ' :nth-child(3)').text();
		console.log(apellido);
		
		return { 
			dni : dni,
			nombre : nombre,
			apellido : apellido
		}
	};
	
	var btn_eliminar_click = function($this, event) {		
		var cliente = obtenerClienteLinea($this);
		
		var text_title = "Baja de cliente";
		var text_body = "¿Estás seguro de dar de baja a " + cliente.nombre + " " + 
						cliente.apellido + " DNI:" + cliente.dni + " ?";
		
		setearVentanaModal(text_title, text_body);
		
		$contenedor_modal.off();
		$contenedor_modal.on('click', '.btn-primary', function(event) {
			confirmar_baja_click(cliente.dni);
		});
	};
	
	var cliente_post = function(cliente){
		var text_title = "Mensaje de Información";
		if (cliente == "si") {
			text_body = "La transacción ha sido exitosa";
		} else {
			text_body = "Ha ocurrido un error durante la transacción";
		}
		setearVentanaModal(text_title, text_body);
	};
	
	var confirmar_baja_click = function(dni) {
		$.post(base_url + 'Fachada_cliente/bajar_cliente',
			{ dni : dni },
			function(cliente){
				cliente_post(cliente);
				table.ajax.reload();
			}
		);
		$contenedor_modal.off();
		$contenedor_modal.on('click', '.btn-primary', function(event) {
			$ventanaModal.modal('toggle');
			window.location.reload(true);
		});
	};
	
	var binds = function() {
		console.log("binds");
		
		$my_id.on('click', '.btn-danger', function(event) {
			event.preventDefault();
			var $this = $(this);
			console.log("bind eliminar");
			btn_eliminar_click($this, event);
		});
		
		$contenedor_modal.on('click', '.btn-primary', function(event) {
			confirmar_baja_click;
		});
	};
	
	var init = function() {
		console.log("init");
		inicializar();
		binds();
	};
	
	return { init : init }
})();

$(document).ready(function() {
	dar_baja_cliente.init();
});
</script>
<style>
@font-face {
    font-family: 'myfont';
    src: url(/sgf/assets/fonts/Arciform.otf);
}
#titulo{
font-family: myfont;
}

</style>