<?php 
$rol = $this->session->userdata('rol');

if (in_array($rol, $roles)) {
    $this->load->view('includes/Header');
    switch ($rol) {
        case "cliente":
            $this->load->view('sucursales');
            break;
        case "vendedor":
            $this->load->view('botones_vendedor');
            if ($sucursal === TRUE) {
                $this->load->view('sucursales');
            }
            break;
        case "administrador":
            $this->load->view('botones_administrador');
            if ($sucursal === TRUE) {
                $this->load->view('sucursales');
            }
            break;
        case "desarrollador":
            $this->load->view('includes/Header');
            $this->load->view('botones_administrador');
            if ($sucursal === TRUE) {
                $this->load->view('sucursales');
            }
            break;
    }
    $this->load->view($vista);
    $this->load->view('includes/Footer');
} else {
    redirect('login');
}
?>