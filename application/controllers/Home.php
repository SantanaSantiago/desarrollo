<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data = array(
				'roles' => array('vendedor','administrador'),
				'vista' => 'Home',
				'sucursal' => FALSE
		);
		$this->load->view('cargar_pagina', $data);
	}
}
