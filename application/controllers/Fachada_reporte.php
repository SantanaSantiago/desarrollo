<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Fachada_reporte extends CI_Controller {
	
	public function index() {
		echo "hola";
	}
	public function reporte_venta() {
		require_once("assets/reporte/reportico.php");        // Include Reportico
		$q = new reportico();                         // Create instance
		$q->access_mode = "REPORTOUTPUT";             // Allows access to report output only
		$q->initial_execute_mode = "EXECUTE";         // Just executes specified report
		$q->initial_project = "FloreriaReporte";            // Name of report project folder
		$q->initial_report = "Ventas";           // Name of report to run
		$q->bootstrap_styles = "3";                   // Set to "3" for bootstrap v3, "2" for V2 or false for no bootstrap
		$q->force_reportico_mini_maintains = true;    // Often required
		$q->bootstrap_preloaded = true;               // true if you dont need Reportico to load its own bootstrap
		$q->clear_reportico_session = true;           // Normally required
		$q->execute();
	}
}
