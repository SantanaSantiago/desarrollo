<?php 
defined('BASEPATH') or exit('No direct script access allowed');
class Fachada_cliente extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function crearCliente() {
        // $validacion = array(); TO DO array de validación
        $data = array('roles' => array('administrador', 'cliente', 'vendedor'), 'vista' => 'CrearCliente', 'sucursal' => FALSE);
        $this->load->view('cargar_pagina', $data);
    }

    public function modificar_cliente() {
        $data = array('roles' => array('administrador'), 'vista' => 'modificar_cliente', 'sucursal' => FALSE);
        $this->load->view('cargar_pagina', $data);
    }

    public function dar_baja_cliente() {
        $data = array('roles' => array('administrador'), 'vista' => 'dar_baja_cliente', 'sucursal' => FALSE);
        $this->load->view('cargar_pagina', $data);
    }
    public function guardarCliente() {
        $cliente = new Cliente();
        $dni = $this->input->post('dni');
        $nombre = $this->input->post('nombre');
        $apellido = $this->input->post('apellido');
        $mail = $this->input->post('email');
        $direccion = $this->input->post('direccion');
        $telefono = $this->input->post('telefono');
        $clave_de_acceso = $this->input->post('password');

        $guardado = $cliente->guardar($dni, $nombre, $apellido, $direccion, $telefono, $mail, $clave_de_acceso);

        if ($guardado === FALSE) {
            echo '<script type="text/javascript">';
            echo 'alert("Error al intentar guardar el usuario");';
            echo '</script>';
        } else {
            redirect('Fachada_cliente/crearCliente');
        }
    }

    public function formulario_modificacion($dni) {
        $cliente = new Cliente();
        $data = $cliente->get_datos_cliente($dni);
        $data['dni'] = $dni;

        $this->load->view('formularios/form_modificar_cliente', $data);
    }

    public function buscar_clientes($id_boton, $tipo_boton) {
        $cliente = new cliente();
        $cliente->where('habilitado', 1)->get();
        foreach($cliente as $row) {
            $boton = '<a id="'.$id_boton.'" data-toggle="modal" class="btn btn-'.$tipo_boton.'" href="#ventanaModal" >'.$id_boton.'</a>';
            echo '<tr>';
            echo '<td class="text-center">'.$row->dni."</td>";
            echo '<td class="text-center">'.$row->nombre."</td>";
            echo '<td class="text-center">'.$row->apellido."</td>";
            echo '<td class="text-center">'.$row->usuario->get()->mail."</td>";
            echo '<td class="text-center">'.$row->direccion."</td>";
            echo '<td class="text-center">'.$row->telefono."</td>";
            echo '<td class="text-center">'.$boton.'</td>';
            echo '</tr>';
        }
    }

    public function listar() {
        $data = array('roles' => array('administrador', 'vendedor'), 'vista' => 'listar_clientes', 'sucursal' => FALSE);
        $this->load->view('cargar_pagina', $data);
    }

    public function guardar_modificacion() {
        $cliente = new Cliente();
        $usuario = new Usuario();

        $dni = $this->input->post('dni');
        $nombre = $this->input->post('nombre');
        $apellido = $this->input->post('apellido');
        $email = $this->input->post('email');
        $telefono = $this->input->post('telefono');
        $direccion = $this->input->post('direccion');
        $cliente->where('dni', $dni);

        $cliente->update(array('nombre' => $nombre, 'apellido' => $apellido, 'direccion' => $direccion, 'telefono' => $telefono));

        $afectado = $cliente->db->affected_rows();

        $id_usuario = $cliente->where('dni', $dni)->get()->usuario_id;
        $usuario->where('id', $id_usuario)->update('mail', $email);

        $afectado = $afectado + $usuario->db->affected_rows();

        if ($afectado > 0) {
            echo 'si';
        } else {
            echo 'no';
        }
    }
    public function bajar_cliente() {
        $cliente = new Cliente();
        $dni = $this->input->post('dni');
        $cliente->where('dni', $dni)->update('habilitado', 0);
        $afectado = $cliente->db->affected_rows();
        if ($afectado > 0) {
            echo "si";
        } else {
            echo "no";
        }
    }
}