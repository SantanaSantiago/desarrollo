<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vista_Pedidos extends CI_Controller {

	public function index()
	{
		$this->load->view('includes/Header');
		$this->load->view('Botones');
		$this->load->view('Hacer_Pedido');
		$this->load->view('includes/Footer');
	}
}
