<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Factura extends CI_Controller {
	private $id_pedido;
	private $pedido;
	
	// 	'id_pedido' => $id_pedido,
	public function imprimir_factura($id_pedido) {
		$pedido = new Pedido();
		$pedido->get_by_id($id_pedido);
		/*$data = array (
				'roles' => array (
						'vendedor'
				),
				'vista' => 'Factura',
				'sucursal' => FALSE,
				'pedido'=> $pedido
		);*/
		$data = array(
			'pedido'=> $pedido
		);
		$this->load->view('includes/Header');
		$this->load->view('Factura', $data);
		$this->load->view('includes/Footer');
	}
	
	public function detalle_factura() {
		$id_pedido = $this->input->post('id_pedido');
	
		$pedido = new Pedido();
		$pedido->get_by_id($id_pedido);
		
		$linea_pedido = new Linea_pedido();
		$linea_pedido->where('pedido_id', $id_pedido)->get();
		
		foreach ( $linea_pedido as $row ) {
			$precio = $row->precio_producto;
			$cantidad = $row->cantidad;
			$importe = bcmul($precio, $cantidad, "2"); 
		
			echo '<tr>';
			echo '<td>' . $row->producto->get()->nombre_producto . '</td>';
			echo '<td class="text-center">' . $row->producto->get()->tipo_producto . '</td>';
			echo '<td class="text-right">$' . $precio . '</td>';
			echo '<td class="text-right">' . $cantidad . '</td>';
			echo '<td class="text-right">$' . $importe . '</td>';
			echo '</tr>';
		}
		
		echo '<tr><td class="no-line"></td>';
		echo '<td class="no-line"></td>';
		echo '<td class="no-line"></td>';
		echo '<td class="no-line text-center"><strong> Total </strong></td>';
		echo '<td class="no-line text-right"><strong>$'.$pedido->importe.'</strong></td></tr>';
	}
}
