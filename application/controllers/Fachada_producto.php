<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Fachada_producto extends CI_Controller {
	private $validacion;
	private $tipos_producto;
	public function __construct() {
		parent::__construct();

		// Tipos válidos de producto
		$this->tipos_producto = array (
				'flor_natural',
				'flor_artificial',
				'complemento',
				'otro'
		);
	}
	public function alta_producto() {
		$validacion = array (
				array (
						'field' => 'id_nombre',
						'label' => 'Nombre',
						'rules' => 'trim|required|alpha|xss_clean'
				),
				array (
						'field' => 'id_precio',
						'label' => 'Precio',
						'rules' => 'required|decimal|xss_clean'
				),
				array (
						'field' => 'id_descripcion',
						'label' => 'Descripción',
						'rules' => 'trim|required|xss_clean'
				),
				array (
						'field' => 'id_tipo',
						'label' => 'Tipo de Producto',
						'rules' => 'required|callback__validar_tipo|xss_clean'
				),
				array (
						'field' => 'id_path',
						'label' => 'Imagen',
						'rules' => 'required|alpha|xss_clean'
				)
		);

		$data = array(
				'roles' => array('administrador'),
				'vista' => 'alta_producto',
				'sucursal' => FALSE,
				'validacion' => $validacion
		);
		$this->load->view('cargar_pagina', $data);
	}

	/*
	 * Verifica que la cadena pasada como parámetro pertenezca a alguno de los tipos de producto
	 * permitidos
	 */
	public function _validar_tipo($tipo) {
		if (in_array($tipo, $this->tipos_producto, TRUE)) {
			return TRUE;
		} else {
			$this->form_validation->set_message('username_check', 'The %s field tiene que ser de algunos de los tipos permitidos');
			return FALSE;
		}
	}
	public function guardar_producto() {
		$producto = new Producto();

		$nombre = $this->input->post('nombre');
		$precio = $this->input->post('precio');
		$descripcion = $this->input->post('descripcion');
		$tipo = $this->input->post('tipo');

		$mi_archivo = 'imagen';
		$config ['upload_path'] = realpath(APPPATH . "../assets/imagenes");
		$config ['file_name'] = $nombre;
		$config ['allowed_types'] = "jpg";
		$config ['max_size'] = "50000";
		$config ['max_width'] = "20003";
		$config ['max_height'] = "20003";
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('imagen')) {
			$data ['uploadSuccess'] = $this->upload->data();

			$producto->nombre_producto = $nombre;
			$producto->precio = $precio;
			$producto->descripcion = $descripcion;
			$producto->tipo_producto = $tipo;
			$producto->path_img = $nombre . ".jpg";

			if ($producto->save()) {
				$this->alta_producto();
				echo '<script type="text/javascript">';
				echo 'alert("El producto ha sido dado de alta");';
				echo '</script>';
			} else {

				echo '<script type="text/javascript">';
				echo 'alert("Nombre: ' . $nombre .
						'Precio: ' . $precio .
						'Descripcion: ' . $descripcion .
						'Tipo Producto: ' . $tipo .
						'Path: ' . $nombre . '.jpg)';
				echo '</script>';
			}
			// redirect('Fachada_producto/alta_producto');
		} else {
			// *** ocurrio un error
			$data ['uploadError'] = $this->upload->display_errors();
			echo $this->upload->display_errors() . " " . $mi_archivo;
			echo '<script></script>';
		}
	}

	public function consultar_inventario() {
		$data = array(
				'roles' => array('administrador','vendedor'),
				'vista' => 'consultar_inventario',
				'sucursal' => TRUE,
		);
		$this->load->view('cargar_pagina', $data);
	}
	public function formulario_ingreso($sucursal) {
		$data = array(
			'sucursal' => $sucursal
		);
		$this->load->view('formularios/form_registrar_ingreso', $data);
	}
	public function registrar_ingreso() {
		$data = array(
				'roles' => array('administrador'),
				'vista' => 'registrar_ingreso',
				'sucursal' => TRUE,
		);
		$this->load->view('cargar_pagina', $data);
	}
	public function buscar_productos_sucursal($nombre_sucursal) {
		$p = new Producto();
		$i = new Inventario();
		$s = new Sucursal();
		$s->where('nombre_sucursal', $nombre_sucursal)->get()->id;
		$productos = $p->where_related($i, 'sucursal_id', $s);
		$productos->get();
		echo $productos->set_json_content_type();
		echo $productos->all_to_json();
	}

	public function mostrar_productos_tabla() {
		$productos = $this->input->post('productos');
		$sucursal = $this->input->post('sucursal');

		$i = new Inventario();
		$producto = new Producto();
		$boton = '<a id="registrar" data-toggle="modal" class="btn btn-primary" href="#ventanaModal" >Registrar Ingreso</a>';

		foreach ( $productos as $p ) {
			$producto->from_json($p);

			$i->where_related('sucursal', 'nombre_sucursal', $sucursal);
			$i->where_related($producto);
			$i->get();

			$nombre = $producto->nombre_producto;

			echo '<tr>';
			echo '<td>' . $nombre . "</td>";
			echo '<td class="text-center">' . $producto->tipo_producto . "</td>";
			echo '<td class="text-center">' . $producto->descripcion . "</td>";
			echo '<td class="text-center">' . $producto->precio . "</td>";
			echo '<td class="text-center">' . $i->stock . "</td>";
			echo '<td class="text-right">' . $boton . '</td>';
			echo '</tr>';
		}
	}

	public function registrar_ingreso_producto() {
		$vencimiento = "NULL";

		$producto = $this->input->post('producto');
		$stock = $this->input->post('stock');
		$sucursal = $this->input->post('sucursal');
		$vencimiento = $this->input->post('vencimiento');

		$inventario = new Inventario();
		$inventario->where_related('producto', 'nombre_producto', $producto);
		$inventario->where_related('sucursal', 'nombre_sucursal', $sucursal);
		$id_inventario = $inventario->get()->id;

		$lote = new Lote();
		$lote->stock_lote = $stock;
		$lote->fecha_vencimiento = mdate("%Y-%m-%d 00:00:00", strtotime($vencimiento));
		$lote->inventario_id = $id_inventario;
		$lote->oferta_id = 1;
		log_message('error', 'lote: ' . var_export($lote));
		if ($lote->save()) {
			$this->db->query("call sp_nulldates");
		} else {
			echo 'no';
		}
	}
}
