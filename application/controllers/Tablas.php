<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Tablas extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}
	
	public function inventario($sucursal) {
		$this->datatables
		->select('	producto.nombre_producto,
					lote.id,
					lote.fecha_ingreso,
					lote.fecha_vencimiento,
					lote.stock_lote')
		->from('lote')
		->join('inventario', 'inventario.id = lote.inventario_id')
		->join('producto', 'producto.id = inventario.producto_id')
		->join('sucursal', 'sucursal.id = inventario.sucursal_id')
		->where('sucursal.nombre_sucursal = "' . $sucursal . '"');
		
		echo $this->datatables->generate();
	}

	public function productos($sucursal) {
		$boton = '<a id="registrar" data-toggle="modal" class="btn btn-primary"
					href="#ventanaModal" ><i class="glyphicon glyphicon-pencil"></i>  Registrar Ingreso</a>';
		
		$input_cantidad = '<input title="Debe ingresar el stock"
							class="form-control input-sm-4"
							name="stock"
							id="$1" />';
		
		$this->datatables
		->select('	producto.nombre_producto,
					producto.tipo_producto,
					producto.descripcion'	)
		->from('inventario')
		->join('producto', 'producto.id = inventario.producto_id')
		->join('sucursal', 'sucursal.id = inventario.sucursal_id')
		->where('sucursal.nombre_sucursal = "' . $sucursal . '"')
		->add_column('Edit', $boton);
		
		echo $this->datatables->generate();
	}
	
	public function clientes($accion = null) {
		$this->datatables
		->select('	cliente.dni as dni,
					cliente.nombre,
					cliente.apellido,
					usuario.mail,
					cliente.direccion,
					cliente.telefono')
		->from('cliente')
		->join('usuario', 'usuario.id = cliente.usuario_id')
		->where('dni != 0')
		->where('habilitado = 1');
		
		switch($accion) {
			case 'modificar':
				$class = 'warning';
				$nombre = 'Modificar';
				$icon = '<span class="glyphicon glyphicon-edit"></span>';
				break;
			case 'eliminar':
				$class = 'danger';
				$nombre = 'Eliminar';
				$icon = '<span class="glyphicon glyphicon-remove"></span>';
				break;
		}
		
		if($accion !== null) {
			$boton = '<a id="$1" data-toggle="modal" class="btn btn-' . $class . '"
						href="#ventanaModal" >' . $icon . ' ' . $nombre . '</a>';
			$this->datatables->add_column($nombre, $boton, 'dni');
		}
		
		$this->datatables->add_column('identificador', 'row-$1', 'dni');
		
		echo $this->datatables->generate();
	}
	
	public function ventas() {
		$dni = $this->input->post('dni');
		$sucursal = $this->input->post('sucursal');
		$minfecha = $this->input->post('minfecha');
		$maxfecha = $this->input->post('maxfecha');
		
		$this->datatables
		->select('	pedido.id,
					cliente.dni as dni,
					sucursal.nombre_sucursal,
					venta.fecha_hora_venta,
					importe')
		->from('venta')
		->join('pedido', 'pedido.id = venta.pedido_id')
		->join('sucursal', 'sucursal.id = pedido.sucursal_id')
		->join('cliente', 'cliente.id = pedido.cliente_id');
		
		if ($dni !== "") {
			$this->datatables->where('cliente.dni', $dni);
		}
		if ($sucursal !== "") {
			$this->datatables->where('sucursal.nombre_sucursal', $sucursal);
		}
		if ($minfecha !== "") {
			$this->datatables->where('venta.fecha_hora_venta >=', $minfecha);
		}
		if ($maxfecha !== "") {
			$this->datatables->where('venta.fecha_hora_venta <=', $maxfecha);
		}
		
		echo $this->datatables->generate();
	}
}