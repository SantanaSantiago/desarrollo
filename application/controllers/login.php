<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        $this->load->view('includes/Header');
        $this->load->view('login');
        $this->load->view('includes/Footer');
    }

    public function logout() {
        $this->session->sess_destroy();
        $this->index();
    }

    public function autenticar() {
        $mail = $this->input->post('user');
        $pass = $this->input->post('pass');

        $user = new Usuario();

        if ($user->login($mail, $pass)) {
            $rol = $user->rol->get()->nombre_rol;

            $newdata = array('mail' => $mail, 'pass' => $pass, 'rol' => $rol);

            $this->session->set_userdata($newdata);

            echo $rol;
        } else {
            echo 'error';
        }
    }
}