<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Reporte extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}
	
	public function ventas() {
		$data = array(
			'roles' => array('administrador'),
			'vista' => 'reporte_venta',
			'sucursal' => FALSE
		);
		$this->load->view('cargar_pagina', $data);
	}
	public function reporte_ventas(){
		$this->load->view('reporte_ventas');
	}
}