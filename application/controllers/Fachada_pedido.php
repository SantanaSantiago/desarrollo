<?php 
defined('BASEPATH') or exit('No direct script access allowed');
class Fachada_pedido extends CI_Controller {

    public function _obtener_id_cliente() {
        $rol = $this->session->userdata('rol');
        $mail = $this->session->userdata('mail');

        if ($rol === 'cliente') {
            $c = new Cliente();
            return $c->where_related('usuario', 'mail', $mail)->get()->id;
        } else {
            return 1;
        }
    }

    public function realizar_pedido() {
        $data = array('roles' => array('vendedor', 'cliente'), 'vista' => 'realizar_pedido', 'sucursal' => TRUE);
        $this->load->view('cargar_pagina', $data);
    }

    public function crear_pedido() {
        $cliente = $this->input->post('cliente');
        $sucursal = $this->input->post('sucursal');

        $rol = $this->session->userdata('rol');
        $mail = $this->session->userdata('mail');

        $s = new Sucursal();
        $sucursal_id = $s->where('nombre_sucursal', $sucursal)->get()->id;


        if ($rol === 'vendedor') {
            $cliente_id = 1;
        } else {
            $c = new Cliente();
            $cliente_id = $c->where_related_usuario('mail', $mail)->get()->id;
        }

        $pedido = new Pedido();
        $nuevo_pedido = $pedido->crear_pedido($cliente_id, $sucursal_id);

        if ($rol === 'vendedor') {
            echo $nuevo_pedido;
        } else {
            echo "cliente";
        }
    }

    public function mostrar_carrito() {
        $sucursal = $this->input->post('sucursal');

        $id_cliente = $this->_obtener_id_cliente();

        $carritos = new Carrito();
        $carritos->where('cliente_id', $id_cliente);
        $carritos->where_related('inventario/sucursal', 'nombre_sucursal', $sucursal);

        $p = new Pedido();
		$total = "0.00";

        $carritos->get();
        foreach($carritos as $carrito) {
            $cantidad = $carrito->cantidad;
            $producto = $carrito->inventario->get()->producto->get();
            $precio = $p->obtener_precio($producto->precio, $cantidad);
			bcscale(2);
			$total = bcadd($total, $precio);

            $boton_cancelar = '<button type="button" class="btn btn-danger" id="cancelar">'.'Eliminar</button>';

            echo '<tr id="tr">';
            echo '<td width="25%" class="text-left">'.$producto->nombre_producto.'</td>';
            echo '<td width="25%" class="text-right">'.$cantidad.'</td>';
            echo '<td width="25%" class="text-right"> $'.$precio.'</td>';
            echo '<td width="25%" class="text-center">'.$boton_cancelar.'</td>';
            echo '</tr>';
        }
        
		echo '<tr id="tr">';
        echo '<td width="25%" class="no-line"></td>';
        echo '<td width="25%" class="no-line text-right"><strong>Total</strong></td>';
        echo '<td width="25%" class="text-right" style="color:red" id="total"><strong>$' . $total . '</strong></td>';
        echo '<td width="25%" class="no-line"></td>';
        echo '</tr>';
    }

    public function agregar_producto_carrito() {
        $producto = $this->input->post('producto');
        $cantidad = $this->input->post('cantidad');
        $sucursal = $this->input->post('sucursal');

        // Obtiene inventario correspondiente al producto y a la sucursal
        $inventario = new Inventario();
        $inventario->where_related('producto', 'nombre_producto', $producto);
        $inventario->where_related('sucursal', 'nombre_sucursal', $sucursal);
        $inventario->get();

        $stock = $inventario->stock;
        $inventario_id = $inventario->id;


        // Obtiene carrito si existe.
        $carrito = new Carrito();
        $id_cliente = $this->_obtener_id_cliente();
        $carrito->where('cliente_id', $id_cliente);
        $carrito->where('inventario_id', $inventario_id);
        $carrito->get();

        $inventario->trans_begin();
        $carrito->trans_begin();

        // Actualizo stock de inventario
        if ($stock >= $cantidad) {
            $inventario->disminuir_stock($sucursal, $producto, $cantidad);
        } else {
            $inventario->trans_rollback();
            $carrito->trans_rollback();
            echo "Stock Insuficiente";
            return 1;
        }

        if ($carrito->exists()) {
            // Si ya existe el carrito para este inventario y cliente actualizo la cantidad
            $cantidad = $cantidad + $carrito->cantidad;
            $carrito->cantidad = $cantidad;
            $carrito->save();
        } else {
            // Si el carrito es nuevo lo guardo
            $carrito->guardar($id_cliente, $inventario_id, $cantidad);
        }

        $carrito_status = $carrito->trans_status();
        $inventario_status = $inventario->trans_status();

        if ($carrito_status === TRUE && $inventario_status === TRUE) {
            $inventario->trans_commit();
            $carrito->trans_commit();
        } else {
            $inventario->trans_rollback();
            $carrito->trans_rollback();
        }
    }
	
	public function obtener_total_carrito() {
		$sucursal = $this->input->post('sucursal');
		$cliente_id = $this->_obtener_id_cliente();
		$c = new Carrito();
		$total = $c->obtener_total($sucursal, $cliente_id);
		echo $total;
	}

    public function quitar_producto_carrito() {
        // Recibe parámetros por POST
        $producto = $this->input->post('producto');
        $sucursal = $this->input->post('sucursal');

        // Obtiene el inventario asociado al producto y la sucursal
        $inventario = new Inventario();
        $inventario->where_related('sucursal', 'nombre_sucursal', $sucursal);
        $inventario->where_related('producto', 'nombre_producto', $producto)->get();

        // Elimina el carrito del cliente asociado al inventario
        $carrito = new Carrito();
        $carrito->where_related($inventario);
        $id_cliente = $this->_obtener_id_cliente();
        $carrito->where('cliente_id', $id_cliente)->get();

        $inventario->stock = $inventario->stock + $carrito->cantidad;
        $inventario->save();
        $carrito->delete();
    }

    public function consultar_pedidos() {
        $data = array('roles' => array('vendedor'), 'vista' => 'Consultar_Pedidos', 'sucursal' => TRUE);
        $this->load->view('cargar_pagina', $data);
    }

    public function mostrar_pedidos() {
        $nombre_sucursal = $this->input->post('sucursal');

        $sucursal = new Sucursal();
        $pedidos = $sucursal->obtener_pedidos($nombre_sucursal);

        $data = array('pedidos' => $pedidos);

        $this->load->view('tablas/tabla_consultar_pedidos', $data);
    }

    public function ver_pedido($id_pedido) {
        $data = array('roles' => array('vendedor'), 'vista' => 'ver_pedido', 'sucursal' => FALSE, 'id_pedido' => $id_pedido);
        $this->load->view('cargar_pagina', $data);
    }

    public function asentar_venta($id_pedido) {
        $venta = new Venta();
        echo $venta->crear_venta($id_pedido);
    }

    public function mostrar_productos() {
        $productos = $this->input->post('productos');
        $sucursal = $this->input->post('sucursal');

        $producto = new Producto();
        $inventario = new Inventario();
        foreach($productos as $p) {
            $producto->from_json($p);
            $inventario->where_related($producto);
            $inventario->where_related('sucursal', 'nombre_sucursal', $sucursal);
            $inventario->get();

            $nombre_producto = $producto->nombre_producto;

            $precio = $producto->obtener_precio($producto);

            $data = array('nombre_producto' => $nombre_producto, 'precio' => $precio, 'stock' => $inventario->stock, 'path_img' => $producto->path_img);
            log_message('error', 'Fachada_pedido/mostrar_producto: ' . var_export($data, true));
            if ($inventario->stock > 0) {
                $this->load->view('producto', $data);
            }
        }
    }

    public function cargar_pedido() {
        $id_pedido = $this->input->post('id_pedido');

        $p = new Pedido();
        $p->get_by_id($id_pedido);

        $linea = new Linea_pedido();
        $linea->obtener_pedido($id_pedido);

        $data = array('linea_pedido' => $linea, 'pedido' => $p);

        $this->load->view('tablas/tabla_pedido', $data);
    }

    public function contar_productos_carrito() {
        $sucursal = $this->input->post('sucursal');

        $cliente_id = $this->_obtener_id_cliente();

        $carrito = new Carrito();
        $cant_productos = $carrito->contar_productos($sucursal, $cliente_id);

        echo $cant_productos;
    }

    public function obtener_stock() {
        $sucursal = $this->input->post('sucursal');
        $producto = $this->input->post('producto');

        $inventario = new Inventario();
        $stock = $inventario->obtener_stock($sucursal, $producto);
        echo $stock;
    }

    public function disminuir_stock() {
        $sucursal = $this->input->post('sucursal');
        $producto = $this->input->post('producto');
        $cantidad = $this->input->post('cantidad');
    }

    public function validar_stock() {
        $sucursal = $this->input->post('sucursal');
        $cantidad = $this->input->post('cantidad');
        $p = $this->input->post('producto');

        $producto = new Producto();
        $inventario = new Inventario();

        $producto->where('nombre_producto', $p)->get();
        $inventario->where_related($producto);
        $inventario->where_related('sucursal', 'nombre_sucursal', $sucursal);
        $inventario->get();

        if ($cantidad >= $inventario->stock) {
            echo json_encode(false);
        } else {
            echo json_encode(true);
        }
    }
}