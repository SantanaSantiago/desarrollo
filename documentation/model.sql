-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: desarrollo
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `arreglo`
--

DROP TABLE IF EXISTS `arreglo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arreglo` (
  `arreglo_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arreglo`
--

LOCK TABLES `arreglo` WRITE;
/*!40000 ALTER TABLE `arreglo` DISABLE KEYS */;
/*!40000 ALTER TABLE `arreglo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carrito`
--

DROP TABLE IF EXISTS `carrito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carrito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `inventario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carrito`
--

LOCK TABLES `carrito` WRITE;
/*!40000 ALTER TABLE `carrito` DISABLE KEYS */;
/*!40000 ALTER TABLE `carrito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('ff2c63677a464121900b03c8087df859','::1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0',1458148656,'a:4:{s:9:\"user_data\";s:0:\"\";s:4:\"mail\";s:23:\"administrador@gmail.com\";s:4:\"pass\";s:4:\"1234\";s:3:\"rol\";s:13:\"administrador\";}');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` int(11) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `habilitado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,0,'Consumidor Final','','','',2,1),(2,123,'Alejandro','Solis','Mitre','(0297) 448-3596',1,0),(4,35659024,'Santiago','Santana','Houssay 2461','(0297) 446-3596',4,0),(5,36906528,'Martin','Gimenez','Codigo 2399','(0297) 472-2546',5,0);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` varchar(45) DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  `coste_mensual` decimal(10,0) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `cuenta_corriente_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato_has_movimiento`
--

DROP TABLE IF EXISTS `contrato_has_movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato_has_movimiento` (
  `contrato_id` int(11) DEFAULT NULL,
  `movimiento_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato_has_movimiento`
--

LOCK TABLES `contrato_has_movimiento` WRITE;
/*!40000 ALTER TABLE `contrato_has_movimiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato_has_movimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuenta_corriente`
--

DROP TABLE IF EXISTS `cuenta_corriente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuenta_corriente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limite_credito` decimal(10,0) DEFAULT NULL,
  `saldo` decimal(10,0) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuenta_corriente`
--

LOCK TABLES `cuenta_corriente` WRITE;
/*!40000 ALTER TABLE `cuenta_corriente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cuenta_corriente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrega`
--

DROP TABLE IF EXISTS `entrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrega` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_entrega` datetime DEFAULT NULL,
  `contrato_id` int(11) DEFAULT NULL,
  `inventario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrega`
--

LOCK TABLES `entrega` WRITE;
/*!40000 ALTER TABLE `entrega` DISABLE KEYS */;
/*!40000 ALTER TABLE `entrega` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventario`
--

DROP TABLE IF EXISTS `inventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `habilitado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventario`
--

LOCK TABLES `inventario` WRITE;
/*!40000 ALTER TABLE `inventario` DISABLE KEYS */;
INSERT INTO `inventario` VALUES (19,12,7,1,1),(20,23,7,2,1),(21,34,7,3,1),(25,44,9,1,1),(26,5,9,2,1),(27,0,9,3,1),(28,166,10,1,1),(29,6,10,2,1),(30,0,10,3,1);
/*!40000 ALTER TABLE `inventario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linea_pedido`
--

DROP TABLE IF EXISTS `linea_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linea_pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `precio_producto` decimal(10,0) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `pedido_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linea_pedido`
--

LOCK TABLES `linea_pedido` WRITE;
/*!40000 ALTER TABLE `linea_pedido` DISABLE KEYS */;
INSERT INTO `linea_pedido` VALUES (20,10,4,51,7),(21,10,5,52,7),(22,14,9,52,9),(23,10,6,53,7),(24,14,3,53,9),(26,10,33,54,7),(27,10,6,55,7),(28,20,1,56,10),(29,10,6,57,7),(30,14,1,58,9),(31,14,2,59,9),(32,20,1,59,10),(34,10,3,60,7),(35,14,4,60,9),(37,10,4,61,7),(38,14,1,61,9),(39,20,3,62,10),(40,20,3,63,10),(41,20,3,64,10),(42,10,1,65,7),(43,20,3,65,10),(45,20,6,66,10),(46,20,3,67,10),(47,20,4,68,10),(48,20,4,69,10),(49,20,4,70,10),(50,20,5,71,10),(51,10,5,72,7),(52,10,6,73,7),(53,10,3,74,7),(54,10,49,75,7),(55,10,9,76,7);
/*!40000 ALTER TABLE `linea_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lote`
--

DROP TABLE IF EXISTS `lote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_lote` int(11) DEFAULT NULL,
  `fecha_ingreso` datetime DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  `oferta_id` int(11) DEFAULT NULL,
  `inventario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lote`
--

LOCK TABLES `lote` WRITE;
/*!40000 ALTER TABLE `lote` DISABLE KEYS */;
INSERT INTO `lote` VALUES (16,10,'2015-11-20 13:17:56','2015-08-14 00:00:00',1,20),(19,4,'2015-11-20 13:29:31','2015-12-10 00:00:00',1,25),(21,5,'2015-11-20 13:30:52','2015-12-19 00:00:00',1,26),(22,6,'2015-11-20 13:31:07','2015-11-20 00:00:00',1,29),(23,13,'2015-11-20 13:31:45','2016-03-11 00:00:00',1,20),(24,30,'2015-11-20 16:38:01','2015-11-30 00:00:00',1,21),(25,4,'2015-11-20 16:38:43','2015-12-09 00:00:00',1,21),(46,19,'2016-03-10 13:58:25','2016-03-10 00:00:00',1,28),(50,40,'2016-03-10 15:56:13','2016-03-27 00:00:00',1,25),(51,123,'2016-03-15 10:34:10','2016-03-15 00:00:00',1,28),(52,12,'2016-03-15 17:46:34','2016-03-16 00:00:00',1,19),(53,12,'2016-03-15 17:47:52','2016-03-15 00:00:00',1,28),(54,12,'2016-03-15 18:57:31','2016-03-15 00:00:00',1,28);
/*!40000 ALTER TABLE `lote` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `desarrollo`.`lote_AFTER_INSERT` AFTER INSERT ON `lote` FOR EACH ROW
BEGIN
SET SQL_SAFE_UPDATES = 0;
	UPDATE desarrollo.inventario
    SET inventario.stock = inventario.stock + NEW.stock_lote
    WHERE inventario.id = NEW.inventario_id;
SET SQL_SAFE_UPDATES = 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `desarrollo`.`lote_BEFORE_DELETE` BEFORE DELETE ON `lote` FOR EACH ROW
BEGIN

SET SQL_SAFE_UPDATES = 0;
	UPDATE desarrollo.inventario
    SET inventario.stock = inventario.stock - OLD.stock_lote
    WHERE inventario.id = OLD.inventario_id;
SET SQL_SAFE_UPDATES = 1;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `lotes`
--

DROP TABLE IF EXISTS `lotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lotes` (
  `id` int(11) NOT NULL,
  `inventario_id` int(11) DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lotes`
--

LOCK TABLES `lotes` WRITE;
/*!40000 ALTER TABLE `lotes` DISABLE KEYS */;
INSERT INTO `lotes` VALUES (16,20,'2015-08-14 00:00:00'),(18,19,'2015-12-04 00:00:00'),(19,25,'2015-12-10 00:00:00'),(20,28,'2015-11-20 00:00:00'),(21,26,'2015-12-19 00:00:00'),(22,29,'2015-11-20 00:00:00'),(24,21,'2015-11-30 00:00:00');
/*!40000 ALTER TABLE `lotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento`
--

DROP TABLE IF EXISTS `movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_hora` datetime DEFAULT NULL,
  `importe` decimal(10,0) DEFAULT NULL,
  `tipo_movimiento` varchar(12) DEFAULT NULL,
  `cuenta_corriente_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento`
--

LOCK TABLES `movimiento` WRITE;
/*!40000 ALTER TABLE `movimiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento_has_venta`
--

DROP TABLE IF EXISTS `movimiento_has_venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimiento_has_venta` (
  `movimiento_id` int(11) DEFAULT NULL,
  `venta_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento_has_venta`
--

LOCK TABLES `movimiento_has_venta` WRITE;
/*!40000 ALTER TABLE `movimiento_has_venta` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimiento_has_venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oferta`
--

DROP TABLE IF EXISTS `oferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` datetime DEFAULT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `descuento` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oferta`
--

LOCK TABLES `oferta` WRITE;
/*!40000 ALTER TABLE `oferta` DISABLE KEYS */;
INSERT INTO `oferta` VALUES (1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0);
/*!40000 ALTER TABLE `oferta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido`
--

DROP TABLE IF EXISTS `pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `importe` decimal(10,0) DEFAULT NULL,
  `fecha_hora` datetime DEFAULT NULL,
  `valor_iva` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido`
--

LOCK TABLES `pedido` WRITE;
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;
INSERT INTO `pedido` VALUES (51,48,'2015-11-20 13:25:03',21,1,2),(52,204,'2015-11-20 13:33:18',21,1,2),(53,120,'2015-11-20 14:40:40',21,1,1),(54,396,'2015-11-20 15:05:50',21,1,2),(55,72,'2015-11-20 16:25:52',21,2,1),(56,24,'2015-11-20 16:26:22',21,2,2),(57,72,'2015-11-20 16:39:36',21,1,3),(58,16,'2015-11-20 16:43:41',21,2,1),(59,56,'2015-11-20 22:30:53',21,2,1),(60,100,'2015-11-20 23:43:52',21,2,1),(61,64,'2015-11-20 23:51:27',21,2,2),(62,72,'2016-01-03 21:59:01',21,1,1),(63,72,'2016-01-03 21:59:58',21,1,1),(64,72,'2016-01-03 22:07:31',21,2,1),(65,84,'2016-01-24 16:58:28',21,1,2),(66,144,'2016-01-24 16:59:26',21,2,2),(67,72,'2016-03-10 01:22:10',21,1,1),(68,96,'2016-03-10 11:23:56',21,1,1),(69,96,'2016-03-10 11:32:31',21,1,1),(70,96,'2016-03-10 11:34:32',21,1,1),(71,120,'2016-03-10 14:00:23',21,1,1),(72,60,'2016-03-10 15:52:29',21,1,1),(73,72,'2016-03-10 15:53:31',21,1,1),(74,36,'2016-03-10 16:10:52',21,1,1),(75,588,'2016-03-10 16:12:08',21,1,1),(76,108,'2016-03-10 17:38:11',21,1,1);
/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `desarrollo`.`pedido_BEFORE_INSERT` BEFORE INSERT ON `pedido` FOR EACH ROW
thisTrigger: BEGIN
	IF (@disable_triggers) THEN
		LEAVE thisTrigger;
	END IF;
        
	SET SQL_SAFE_UPDATES = 0;
	
    SET NEW.fecha_hora = NOW();
        
	SET SQL_SAFE_UPDATES = 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `desarrollo`.`pedido_AFTER_INSERT` AFTER INSERT ON `pedido` FOR EACH ROW
BEGIN
	SET SQL_SAFE_UPDATES = 0;
    
	INSERT INTO linea_pedido(precio_producto, cantidad, pedido_id, producto_id)
	SELECT producto.precio, carrito.cantidad, NEW.id as pedido_id, inventario.producto_id
    FROM carrito
    INNER JOIN inventario
		ON carrito.inventario_id = inventario.id
	INNER JOIN producto
		ON inventario.producto_id = producto.id
	WHERE inventario.sucursal_id = NEW.sucursal_id 
		AND carrito.cliente_id = NEW.cliente_id;
        
	DELETE 
    FROM carrito 
    USING carrito INNER JOIN inventario 
	ON carrito.inventario_id = inventario.id
    WHERE 
		carrito.cliente_id = NEW.cliente_id 
		AND inventario.sucursal_id = NEW.sucursal_id;
	SET SQL_SAFE_UPDATES=1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `desarrollo`.`pedido_AFTER_DELETE` AFTER DELETE ON `pedido` FOR EACH ROW
thisTrigger: BEGIN
	IF (@disable_triggers) THEN
		LEAVE thisTrigger;
	END IF;

SET SQL_SAFE_UPDATES = 0;

UPDATE inventario
INNER JOIN linea_pedido
	ON inventario.producto_id = linea_pedido.producto_id
SET inventario.stock = inventario.stock + linea_pedido.cantidad
WHERE inventario.sucursal_id = OLD.sucursal_id
    AND linea_pedido.pedido_id = OLD.id;
    
SET SQL_SAFE_UPDATES = 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `precio` decimal(10,0) DEFAULT NULL,
  `nombre_producto` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `tipo_producto` varchar(45) DEFAULT NULL,
  `path_img` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (7,10,'Rosa','Rosa','Flor Natural','Rosa.jpg'),(9,14,'Tulipan','Tulipan','Flor Natural','Tulipan.jpg'),(10,20,'Coronilla','coronilla','Flor Artificial','Coronilla.jpg');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `desarrollo`.`producto_AFTER_INSERT` AFTER INSERT ON `producto` FOR EACH ROW
BEGIN
	SET SQL_SAFE_UPDATES = 0;
		
	INSERT INTO inventario(stock, producto_id, sucursal_id, habilitado)
	SELECT 0, NEW.id, sucursal.id, 1
	FROM sucursal;
        
	SET SQL_SAFE_UPDATES=1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `inventario_id` int(11) DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_rol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'cliente'),(2,'vendedor'),(3,'administrador');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_sucursal` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursal`
--

LOCK TABLES `sucursal` WRITE;
/*!40000 ALTER TABLE `sucursal` DISABLE KEYS */;
INSERT INTO `sucursal` VALUES (1,'Sucursal1','lavalle123',0,'suc@gmail.com'),(2,'Sucursal2','bla',0,'bla@gmail.com'),(3,'Sucursal3','pepe',4324,'asd@gmail.com');
/*!40000 ALTER TABLE `sucursal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(45) DEFAULT NULL,
  `clave_de_acceso` varchar(45) DEFAULT NULL,
  `rol_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'alejandro@gmail.com','1234',1),(2,'vendedor@hotmail.com','1234',2),(3,'administrador@gmail.com','1234',3),(4,'santana.santiago@gmail.cm','1234',1),(5,'jm.gimenez@gmail.com','1234',1),(6,'f.alberto@yahoo.com','alabarda',1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_hora_venta` datetime DEFAULT NULL,
  `pedido_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (26,'2015-11-20 13:25:06',51),(27,'2015-11-20 13:33:59',52),(32,'2015-11-20 16:29:14',55),(33,'2015-11-20 16:30:03',56),(34,'2015-11-20 16:39:42',57),(35,'2015-11-20 16:45:31',58),(36,'2015-11-20 22:31:19',59),(37,'2015-11-20 23:44:11',60),(38,'2015-11-20 23:51:47',61),(39,'2016-01-03 21:59:08',62),(40,'2016-01-03 22:00:04',63),(41,'2016-01-03 22:07:57',53),(42,'2016-01-24 16:57:46',64),(43,'2016-01-24 16:58:34',65),(44,'2016-02-29 14:41:39',54),(45,'2016-03-10 01:22:18',67),(46,'2016-03-10 11:24:18',68),(47,'2016-03-10 11:32:41',69),(48,'2016-03-10 11:36:06',70),(49,'2016-03-10 14:00:33',71),(50,'2016-03-10 15:52:32',72),(51,'2016-03-10 15:53:34',73),(52,'2016-03-10 16:10:56',74),(53,'2016-03-10 16:12:11',75),(54,'2016-03-10 17:38:17',76);
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `desarrollo`.`venta_BEFORE_INSERT` BEFORE INSERT ON `venta` FOR EACH ROW
thisTrigger: BEGIN
	IF (@disable_triggers) THEN
		LEAVE thisTrigger;
	END IF;
    
	SET SQL_SAFE_UPDATES = 0;
	
    SET NEW.fecha_hora_venta = NOW();
        
	SET SQL_SAFE_UPDATES = 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'desarrollo'
--

--
-- Dumping routines for database 'desarrollo'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_actualizar_lotes_venta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualizar_lotes_venta`(IN id_pedido INTEGER)
BEGIN
	DECLARE id_inventario INTEGER;
    DECLARE cantidad INTEGER;
    DECLARE done INTEGER DEFAULT FALSE;

	DECLARE cur1 CURSOR FOR
    SELECT inventario.id, linea_pedido.cantidad
    FROM desarrollo.pedido
    INNER JOIN desarrollo.linea_pedido
		ON pedido.id = linea_pedido.pedido_id
	INNER JOIN desarrollo.inventario
		ON (pedido.sucursal_id = inventario.sucursal_id
        AND linea_pedido.producto_id = inventario.producto_id)
	WHERE pedido.id = id_pedido;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=1;
    
    OPEN cur1;
    lineas: LOOP
		FETCH cur1 INTO id_inventario, cantidad;
		IF done = 1 THEN LEAVE lineas; END IF;
        CALL sp_quitar_elementos_inventario(id_inventario, cantidad);
	END LOOP;
    CLOSE cur1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_quitar_elementos_inventario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_quitar_elementos_inventario`(IN inv_id INTEGER, IN cantidad INTEGER)
BEGIN
	DECLARE lote_id INTEGER;
	DECLARE resto INTEGER;
    DECLARE cant INTEGER DEFAULT cantidad;
    
	SET SQL_SAFE_UPDATES = 0;
    
    UPDATE inventario
    SET inventario.stock = inventario.stock - cantidad
    WHERE inventario.id = inv_id;
    
    quitar_elementos: LOOP
		SELECT lote.id
		INTO lote_id
		FROM desarrollo.lote
		WHERE lote.inventario_id = inv_id
		ORDER BY lote.fecha_vencimiento ASC
		LIMIT 1;
		
		CALL sp_quitar_elementos_lote(lote_id, cant, resto);
        
        IF resto = 0 THEN
			LEAVE quitar_elementos;
		ELSE
			SET cant = resto;
			SET resto = NULL;
		END IF;
    END LOOP quitar_elementos;
    
    SET SQL_SAFE_UPDATES = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_quitar_elementos_lote` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_quitar_elementos_lote`(IN id_lote INTEGER, IN cant INTEGER, OUT resto INTEGER)
BEGIN
	DECLARE stock INTEGER;
    DECLARE dif INTEGER;
    
    SELECT lote.stock_lote INTO stock FROM desarrollo.lote WHERE lote.id = id_lote;
    SET dif = ABS(cant - stock);
    
    SET SQL_SAFE_UPDATES = 0;
	UPDATE desarrollo.lote
    SET stock_lote = IF(stock <= cant, 0, dif)
    WHERE id = id_lote;
    
    DELETE FROM desarrollo.lote WHERE stock_lote = 0 LIMIT 1;
    
    SET resto = IF(stock < cant, dif, 0);
    
    SET SQL_SAFE_UPDATES = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-16 15:44:04